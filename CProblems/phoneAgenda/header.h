#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

typedef struct
{
    char nume[20];
    char prenume[20];
    char adresa[40];
    char stat[20];
    char email[40];
    int tel;
}carte;
void realocaCarte(carte **pagini,int n);
void citireInregistrare(carte *p,FILE*f);
void citireNrTel(carte *p);
void afisareInregistrare(carte *p);
void afisarePaginiAurii(carte *pagini,int n);
void adaugare(carte**pagini,int *n,FILE*f);
int gasesteIndice(carte *pagini,int n, carte p);
void stergere(carte **pagini,int *n,carte p);
void gasesteNume(carte *pagini,int n,char *nume);



#endif // HEADER_H_INCLUDED
