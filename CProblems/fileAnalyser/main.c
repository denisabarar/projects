#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_CUV 20
#define MAX_CAR_LINIE 50
#define MAX_LINII 100
#define MAX_TERMENI 200
/*4. Analizor de fișier.
Scrieți codul pentru un analizor (index) al apariției cuvintelor dintr-un fișier care afișează fiecare cuvânt și numărul liniilor din fișier
în care apare acel cuvânt.
Aplicația să permită afișarea celor mai frecvente N cuvinte din fișier (N furnizat de utilizator).*/
typedef struct termen
{
    char cuvant[MAX_CUV];
    int v[MAX_LINII];
    int i_v;
}termen;
int cauta_termen(termen t[], int n, char s[])
{
    for(int i=0;i<n;i++)
        if(strcmp(t[i].cuvant,s)==0)
            return i;
    return -1;
}

void creeaza_termen(termen *t, int *n, char s[], int linie)
{
    termen *ter=(termen*)malloc(sizeof(termen));
    strcpy(ter->cuvant,s);
    ter->i_v=0;
    ter->v[ter->i_v]=linie;
    (ter->i_v)++;
    (*n)++;
}

void afisare(termen *t, int n)
{
    for(int i=0;i<n;i++)
    {
        printf("\ncuvantul %s apare de %d ori pe liniile: ",t[i].cuvant,t[i].i_v);
        for(int j=0;j<t[i].i_v;j++)
            printf("%d ",t[i].v[j]);
    }
}
void stergere_ultimul_cuv(termen *t,int n)
{
    t[n-1].v[t[n-1].i_v-1]=0;
    t[n-1].i_v--;
}
void parcurge(FILE *f, termen *t, int *n)
{
    char *lstring=(char*)malloc(MAX_CAR_LINIE);
    char *s=(char*)malloc(MAX_CUV);
    int linie=0;
    while(!feof(f))
    {
        fgets(lstring,MAX_CAR_LINIE,f);            /*cu se evita citirea ultimului cuv*/
        //fscanf(f,"%50s ",lstring);              /***in loc de 50 cum se pune constanta MAX_CAR_LINIE?***/
        linie++;
        int cantitate=0;
        while(sscanf(lstring+cantitate,"%s",s)==1)
        {
            cantitate+=strlen(s)+1;
            int i=cauta_termen(t,*n,s);
            if(i!=-1)
            {
                (t+i)->v[(t+i)->i_v]=linie;
                (t+i)->i_v++;
            }
            else//creeam alt termen
            {
                strcpy(t[*n].cuvant,s);
                t[*n].i_v=0;
                t[*n].v[t[*n].i_v]=linie;
                (t[*n].i_v)++;
                (*n)++;
            }
        }
    }
    stergere_ultimul_cuv(t,*n);
}
void sortare_nr_aparitii(termen *t, int n)
{
    int ok=0;
    do
    {
        ok=0;
        for(int i=0;i<n-1;i++)
            if(t[i].i_v<t[i+1].i_v)
            {
                termen aux=t[i];
                t[i]=t[i+1];
                t[i+1]=aux;
                ok=1;
            }
        n--;
    }
    while(ok);
}
int main()
{
    FILE* f;
    int n=0;
    int N;
    f=fopen("date.in","r");
    if(f==NULL)
    {
        printf ("Fisierul nu se poate deschide!");
        exit(1);
    }
    termen *t=(termen*)malloc(MAX_TERMENI*sizeof(termen));
    parcurge(f,t,&n);
    afisare(t,n);
    printf("\ndati N"); scanf("%d",&N);
    sortare_nr_aparitii(t,n);
    afisare(t,N);
    fclose(f);
    return 0;
}
