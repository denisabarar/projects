#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"
/*Pagini aurii. Implementați o aplicație care simulează o carte de telefoane cu următoarele funcționalități:
 adăugarea unei noi înregistrări (nume, prenume, adresa, nr telefon)
 ștergerea unei înregistrări
 căutarea unui număr de telefon pe baza numelui + prenumelui
 căutarea informațiilor pe baza numărului de telefon
 Afișarea tuturor înregistrărilor pe baza numelui de familie dat
 Afișarea tuturor informațiilor din cartea de telefoane în ordinea alfabetică a numelui și prenumelui. Afișarea se
va face pagină cu pagină (utilizatorul apasă o tastă pentru a continua afișarea următoarei pagini).*/


int main()
{
    int n=1;
    carte *pagini;
    pagini=alocaCarte(n);
    carte p;
    int i;

    char comanda[20];
    int nr_c;
    FILE*f=fopen("fill.in","r");
    fscanf(f,"%s",comanda);
    fscanf(f,"%d",&nr_c);
    while(nr_c>0)
    {
        adaugare(&pagini,&n,f);
        nr_c--;
    }
    afisarePaginiAurii(pagini,n);
}

/*
int main()
{
    int c;
    int n=1;
    carte *pagini;
    pagini=alocaCarte(n);
    carte p;
    int i;
    char nume[100];
    printf("1-adaugare\t 2-stergere\t 3-cautareNumePrenume\t 4-cautareNrTel\t 5-afisareInregNumeDeFamilie\t 6-afisareAll\n");
    do
    {
        printf("introduceti comanda: ");
        scanf("%d",&c);
        switch(c)
        {
        case 1:
            adaugare(&pagini,&n);
            break;
        case 2:
            citireInregistrare(&p);
            stergere(&pagini,&n,p);
            break;
        case 3:
            citireInregistrare(&p);
            i=gasesteIndice(pagini,n,p);
            p=pagini[i];
            afisareInregistrare(&p);
            break;
        case 4:
            citireNrTel(&p);
            i=gasesteIndice(pagini,n,p);
            p=pagini[i];
            afisareInregistrare(&p);
            break;
        case 5:
            printf("dati nume de familie:");
            scanf("%s",nume);
            gasesteNume(pagini,n,nume);
            break;
        case 6:
            afisarePaginiAurii(pagini,n);
            break;
        default: break;
        }
    }
    while(c!=-1);
    return 0;
}*/
