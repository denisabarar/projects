#include <stdio.h>
#include <stdlib.h>
/*6. Copierea unui fisier text sursă într-un fișier destinație in ordinea inversă a caracterelor.
(numele fișierelor sunt specificate din linia de comandă) */
int main(int argc, char **argvs)
{
    FILE *f,*g;
    f=fopen(argvs[1],"r");
    g=fopen(argvs[2],"w");
    if(f==NULL)
    {
        printf("Fisierul nu s-a putut deshide!");
        exit(1);
    }
    if(g==NULL)
    {
        printf("Fisierul nu s-a putut deshide!");
        exit(1);
    }
    char c;
    fseek(f,-3l,SEEK_END);//ma pozitionez la sfarsit
    int n=ftell(f);
    printf("%d",n);
    while(ftell(f)>0)
    {
        c=fgetc(f);
        fputc(c,g);
        if(c==10)
            fseek(f,-3l,SEEK_CUR);
        else
            fseek(f,-2l,SEEK_CUR);
    }
    c=fgetc(f);//pentru primul caracter
    fputc(c,g);

    fclose(f);
    fclose(g);
    return 0;
}
