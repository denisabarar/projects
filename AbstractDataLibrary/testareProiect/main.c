#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "testerHeader.h"

#define INT 4
#define CHAR 1
#define FLOAT 8

void* determinaStructura(VectorStructures vs[10], int sizevs, char c)
{
    for(int i=0; i < sizevs; i++)
    {
        if(vs[i].denumire == c)
            return vs[i].structura;
    }
    return NULL;
}

int determinaPozBST(VectorStructures vs[10], int sizevs, char c)
{
    for(int i=0; i < sizevs; i++)
    {
        if(vs[i].denumire == c)
            return i;
    }
    return -1;
}
int main(int argc, char** argv)
{
    //VectorInt(argc, argv, 1, INT);
    //VectorFloat(argc, argv, 2, FLOAT);
    //VectorChar(argc, argv, 3, CHAR);

    //ListInt(argc, argv, 4);

    //HashTableInt(argc, argv, 5, INT);

    //MinHeapInt(argc,argv,6, INT);

    //BST_Int2(argc, argv, 7);
    //BST_Char(argc, argv, 9);
    //BST_String(argc, argv, 10);

    //BalancedBST_Int2(argc, argv, 8);
    return 0;
}
