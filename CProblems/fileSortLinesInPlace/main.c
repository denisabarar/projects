#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_S 100
/*8. Sortarea liniilor dintr-un fișier text. Scrieți programul C pentru sortarea liniilor dintr-un fișier text în următoarele moduri:
 În ordine alfabetică
 În ordinea crescătoare a numărului de cuvinte de pe linie
 În ordinea crescătoare a numărului de caractere de pe linie*/
void sortare_insertie(int v[],int n)
{
    for(int i=1;i<n;i++)
    {
        for(int j=i;j>0&&v[j-1]>v[j];j--)
        {
            int aux=v[j];
            v[j]=v[j-1];
            v[j-1]=aux;
        }
    }
}

int numar_cuvinte(char *s)
{
    int k=0;
    for(char *p=s;p<s+strlen(s);p++)
        if(*p==' ')
            k++;
    return k+1;
}
int criteriu(int c,char *s1,char *s2)
{
    int nrc1,nrc2;
    switch(c)
    {
    case 1://alfabetic
        return strcmp(s1,s2);
    case 2://nr de cuv
        nrc1=numar_cuvinte(s1);
        nrc2=numar_cuvinte(s2);
        return nrc1-nrc2;
    case 3:
        nrc1=strlen(s1);
        nrc2=strlen(s2);
        return nrc1-nrc2;
    }
    return 0;
}
int nr_linii(FILE *f,int**p)
{
    fseek(f,0,SEEK_SET);
    int k=0;
    char c;
    while(!feof(f))
    {
        c=fgetc(f);
        if(c=='\n')
        {
            *(*p+k)=(int)f;
            k++;
        }
    }
    fseek(f,0,SEEK_SET);
    return k;
}
void seteaza_cursor_la_linie(FILE *f, int linie)
{
    fseek(f,0,SEEK_SET);
    int k=0;
    char c;
    while(!feof(f)&&(k<linie))
    {
        c=fgetc(f);
        if(c=='\n')
            k++;
    }
}
char* citeste_linia(FILE *f, int linie)
{
    seteaza_cursor_la_linie(f,linie);
    char *s=(char*)malloc(MAX_S);
    fscanf(f,"%[^\n]s",s);
    fseek(f,0,SEEK_SET);
    return s;
}
void interschimba(FILE *f, char *s1, char *s2, int l1, int l2)//l1,l2 liniile
{
    long slen1,slen2,dif,i;
    char c;
    slen1=strlen(s1);
    slen2=strlen(s2);
    dif=slen1-slen2;
    i=dif;
    if(i<0)
    {
        seteaza_cursor_la_linie(f,l2);
        fseek(f,-dif,SEEK_CUR);
        while(i<0)
        {
            c=fgetc(f-dif-1);
            fflush(f);
            fprintf(f,"%c",c);
            //**f=*((*f)-dif-1);
            i++;
        }
    }
    else
        if(i>0)
    {
        seteaza_cursor_la_linie(f,l1);
        fseek(f,slen2+1,SEEK_CUR);
        while(i>0)
        {
            c=fgetc(f+dif);
            fprintf(f,"%c",c);
            //**f=*((*f)+dif+1);
            i--;
        }
    }
    seteaza_cursor_la_linie(f,l1);
    fprintf(f,"%s",s2);
    seteaza_cursor_la_linie(f,l2);
    fprintf(f,"%s",s1);
    fseek(f,0,SEEK_SET);
}
char* citeste_linia_p(FILE *f, int **p, int i)
{
    char *s=(char*)malloc(MAX_S);
    f=p[i];
    fscanf(f,"%[^\n]s",s);
    printf("%s\n",s);
    return s;
}
void sortare(FILE*f, int crit,int**p,int n)
{
    char *s1=(char*)malloc(MAX_S);
    char *s2=(char*)malloc(MAX_S);
    int ok=0;
    do
    {
        ok=0;
        for(int i=0;i<n-1;i++)
        {
            s1=citeste_linia_p(f,p,i);
            s2=citeste_linia(f,i+1);
            if(criteriu(crit,s1,s2)>0)
            {
                ok=1;
                interschimba(&f,s1,s2,i,i+1);
            }
        }
    }
    while(ok);
}
int main()
{//liniile incep de la 0
    #define MAX_L 500
    FILE*f=fopen("date.txt","r+");
    int **p=(int**)malloc(MAX_L*sizeof(int*));
    for(int i=0;i<MAX_L;i++)
        p[i]=(int*)malloc(sizeof(int));
    int n=nr_linii(f,p);
   /* for(int i=0;i<n;i++)
        printf("%p\n",p[i]);*/
    fclose(f);
    sortare(f,1,p,n);
    //sortare(2);
    //sortare(3);
    return 0;
}
