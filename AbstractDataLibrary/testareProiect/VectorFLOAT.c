#include "testerHeader.h"

void VectorFloat(int argc, char** argv, int indice, int TYPE)
{
    fisiere f;
    determineFiles(argc,argv,indice,&f);

    char comanda[30];
    char c;
    Vector *v;

    VectorStructures vs[10];
    int sizevs = 0;

    while(!feof(f.f))
    {
        fscanf(f.f,"%s %c ",comanda,&c);

        if(!strcmp(comanda, "CreateVector"))
        {
            v = CreateVector(10,printFloat,cmpNumbers);
            vs[sizevs].structura = v;
            vs[sizevs].denumire = c;
            sizevs++;
        }
        else if(!strcmp(comanda, "PrintVector"))
        {
            v = (Vector*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
                PrintVector(v,f.g);
        }
        else if(!strcmp(comanda, "AddVectorItems"))
        {
            int nritems;
            void *item;

            fscanf(f.f,"%d ",&nritems);
            for(int i=0;i<nritems;i++)
            {
                item=(float*)malloc(TYPE);
                fscanf(f.f,"%g ",(float*)item);

                v = (Vector*)determinaStructura(vs,sizevs, c);
                if(v != NULL)
                    if(AddVectorItem(v,item) == 0)
                        fprintf(f.g,"Nu s-au adaugat elementele\n");
            }
        }
        else if(!strcmp(comanda, "PutVectorItem"))
        {
            int index;
            void *item=(float*)malloc(TYPE);

            fscanf(f.f,"%d ",&index);
            fscanf(f.f,"%g ",(float*)item);

            v = (Vector*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
                if(PutVectorItem(v,item,index) == 0)
                    fprintf(f.g,"Nu s-a pus elementul\n");
        }
        else if(!strcmp(comanda, "GetVectorItem"))
        {
            int index;
            void *item=(float*)malloc(TYPE);

            fscanf(f.f,"%d ",&index);

            v = (Vector*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
            {
                item =(float*)GetVectorItem(v,index);
                if(item != NULL)
                    fprintf(f.g,"%g \n",*((float*)item));
                else
                    fprintf(f.g, "Elementul luat este NULL!\n");
            }
        }
        else if(!strcmp(comanda, "DeleteVectorItem"))
        {
            int index;

            fscanf(f.f,"%d ",&index);

            v = (Vector*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
                if(!DeleteVectorItem(v,index))
                    fprintf(f.g,"Nu s-a sters elementul\n");
        }
        else if(!strcmp(comanda, "SearchVectorItem"))
        {
            int index;
            void *item=(float*)malloc(TYPE);

            fscanf(f.f,"%g ",(float*)item);

            v = (Vector*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
            {
                index=SearchVectorItem(v,item);
                if(index != -1)
                    fprintf(f.g,"%d\n",index);
                fprintf(f.g, "Elementul nu a fost gasit!\n");
            }
        }
        else if(!strcmp(comanda, "SortVector"))
        {
            v = (Vector*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
                SortVector(v);
        }
        else if(!strcmp(comanda, "DeleteVector"))
        {
            v = (Vector*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
            {
                DeleteVector(v);
                for(int i=0; i < sizevs;i++)
                {
                    if(vs[i].denumire == c)
                    {
                        vs[i].structura = NULL;
                        break;
                    }
                }
            }
        }
    }
    fclose(f.f);
    fclose(f.g);
}
