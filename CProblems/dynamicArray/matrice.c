# include "h.h"

int **aloca_matrice(int n,int m)
{
    int **v=(int**)calloc(n,sizeof(int*));
    for(int i=0;i<n;i++)
        v[i]=(int*)calloc(m,sizeof(int));
    if(v==NULL)
    {
        printf("nu s-a alocat matricea cu succes!");
        exit(1);
    }
    return v;
}

void citire_matrice(int **v,int n,int m)
{
    for(int i=0;i<n;i++)
        for(int j=0;j<m;j++)
    {
        //printf("v[%d][%d]= ",i,j);
        //scanf("%d",(*(v+i)+j));
        *(*(v+i)+j)=i+j;
    }
}
void realoca(int ***v,int n,int m,int c)
{
    int**vv=(int**)realloc(*v,n*sizeof(int*));
    if(vv==NULL)
        exit(1);

    for(int i=0;i<n;i++)
    {
        if (i<n-c)
            vv[i]=(int*)realloc(vv[i],m*sizeof(int));
        else
            vv[i]=(int*)malloc(m*sizeof(int));

        if(vv[i]==NULL)
            exit(1);
    }

    *v=vv;
}
void adauga_linie(int ***v,int *n,int m,int linie)
{
    (*n)++;
    realoca(v,*n,m,1);
    for(int i=(*n)-2;i>=linie-1;i--)
        for(int j=m-1;j>=0;j--)
            *(*(*v+i+1)+j)=*(*(*v+i)+j);
    for(int j=0;j<m;j++)
    {
        printf("v[%d][%d]= ",linie-1,j);
        scanf("%d",(*(*v+linie-1)+j));
    }
}
void adauga_coloana(int ***v,int n,int *m,int coloana)
{
    (*m)++;
    realoca(v,n,*m,0);
    for(int j=(*m)-2;j>=coloana-1;j--)
        for(int i=n-1;i>=0;i--)
            *(*(*v+i)+j+1)=*(*(*v+i)+j);
    for(int i=0;i<n;i++)
    {
        printf("v[%d][%d]= ",i,coloana-1);
        scanf("%d",(*(*v+i)+coloana-1));
    }
}
void adauga_inel(int ***v,int *n,int *m)
{
    (*m)+=2;
    (*n)+=2;
    realoca(v,*n,*m,2);
    for(int i=(*n)-3;i>=0;i--)
        for(int j=(*m)-3;j>=0;j--)
    {
        *(*(*v+i+1)+j+1)=*(*(*v+i)+j);
    }
    for(int i=0;i<(*m);i++)//prima linie
    {
        printf("v[%d][%d]= ",0,i);
        scanf("%d",(*(*v+0)+i));
    }
    for(int i=0;i<(*m);i++)//ultima linie
    {
        printf("v[%d][%d]= ",*n-1,i);
        scanf("%d",(*(*v+((*n)-1))+i));
    }
    for(int i=1;i<(*n)-1;i++)//prima coloana
    {
        printf("v[%d][%d]= ",i,0);
        scanf("%d",(*(*v+i)+0));
    }
    for(int i=1;i<(*n)-1;i++)//ultima coloana
    {
        printf("v[%d][%d]= ",i,*m-1);
        scanf("%d",(*(*v+i)+(*m)-1));
    }
}
void afisare_matrice(int **v,int n,int m)
{
    printf("\n");
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
            if((*(v+i)+j)==NULL)
                printf("%d ",'n');
            else
                printf("%d ",(int)*(*(v+i)+j));
        printf("\n");
    }
}
void dealoca_matrice(int **v,int n,int m)
{
    for(int i=0;i<n;i++)
        free(v[i]);
    free(v);
}
