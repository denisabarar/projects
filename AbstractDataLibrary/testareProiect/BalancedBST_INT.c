#include "testerHeader.h"

void BalancedBST_Int(int argc, char** argv, int indice)//indice = al catelea fisier din argv
{/*
    fisiere f;
    determineFiles(argc,argv,indice,&f);

    char comanda[30];
    char c;
    nodeAVL *root;

    VectorStructures vs[10];
    int sizevs = 0;

    while(!feof(f.f))
    {
        fscanf(f.f,"%s %c ",comanda,&c);

        if(!strcmp(comanda, "CreateBalancedBST"))
        {
            root = CreateBalancedBST();
            vs[sizevs].structura = &root;
            vs[sizevs].denumire = c;
            sizevs++;
        }
        else if(!strcmp(comanda, "PrintBalancedBST"))
        {
            root = *(nodeAVL**)determinaStructura(vs, sizevs, c);
            if(root != NULL)
            {
                PrintBalancedBST(root, f.g, printInt, 0);
            }
            fprintf(f.g, "\n");
        }
        else if(!strcmp(comanda, "PreorderBalancedBST"))
        {
            root = *(nodeAVL**)determinaStructura(vs, sizevs, c);
            if(root != NULL)
            {
                PreorderBalancedBST(root, f.g, printInt);
            }
            fprintf(f.g, "\n");
        }
        else if(!strcmp(comanda, "InorderBalancedBST"))
        {
            root = *(nodeAVL**)determinaStructura(vs, sizevs, c);
            if(root != NULL)
            {
                InorderBalancedBST(root, f.g, printInt);
            }
            fprintf(f.g, "\n");
        }
        else if(!strcmp(comanda, "PostorderBalancedBST"))
        {
            root = *(nodeAVL**)determinaStructura(vs, sizevs, c);
            if(root != NULL)
            {
                PostorderBalancedBST(root, f.g, printInt);
            }
            fprintf(f.g, "\n");
        }
        else if(!strcmp(comanda, "AddBalancedBSTItem"))
        {
            int nritems;
            void *data;
            void *key;

            fscanf(f.f, "%d ", &nritems);

            root = *(nodeAVL**)determinaStructura(vs, sizevs, c);
            {
                for(int i=0; i < nritems; i++)
                {
                    data = (int*)malloc(sizeof(void*));
                    key = (int*)malloc(sizeof(void*));
                    fscanf(f.f,"%d ",(int*)data);

                    *((int*)key) = *((int*)data);
                    root = AddBalancedBSTItem(root, key, data, cmpNumbers);
                }
            }
        }
        else if(!strcmp(comanda, "HeightBalancedBST"))
        {
            root = *(nodeAVL**)determinaStructura(vs, sizevs, c);
            if(root != NULL)
                fprintf(f.g, "%d\n", HeightBalancedBST(root));
        }
        else if(!strcmp(comanda, "DeleteBalancedBSTItem"))
        {
            void* key = malloc(sizeof(void*));
            nodeAVL *n;

            fscanf(f.f,"%d ",(int*)key);

            root = *(nodeAVL**)determinaStructura(vs, sizevs, c);
            if(root != NULL)
                n = DeleteBalancedBSTItem(root, key, cmpNumbers);
            if(n != NULL)
                    fprintf(f.g,"%d \n",*((int*)n->data));
                else
                    fprintf(f.g, "Elementul nu a fost gasit ca sa fie sters!\n");
        }
        else if(!strcmp(comanda, "SearchBalancedBSTItem"))
        {
            void *key = malloc(sizeof(void*));
            nodeAVL *n = CreateNodeAVL(key, NULL);

            fscanf(f.f,"%d ",(int*)key);

            root = *(nodeAVL**)determinaStructura(vs,sizevs, c);
            if(root != NULL)
            {
                n = SearchBalancedBSTItem(root, key, cmpNumbers);
                if(n != NULL)
                    fprintf(f.g,"%d \n",*((int*)n->data));
                else
                    fprintf(f.g, "Elementul nu a fost gasit!\n");
            }
        }
        else if(!strcmp(comanda, "DeleteBalancedBST"))
        {
            root = *(nodeAVL**)determinaStructura(vs, sizevs, c);
            if(root != NULL)
            {
                DeleteBalancedBST(root);
                for(int i=0; i < sizevs; i++)
                {
                    if(vs[i].denumire == c)
                    {
                        vs[i].structura = NULL;
                        break;
                    }
                }
            }
        }
    }
    fclose(f.f);
    fclose(f.g);*/
}
