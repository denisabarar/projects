
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

public class FourInARowModel {

	private static final int FRAME_THICKNESS = 160;
	private static final int GRID_WIDTH = 4;

	private Cell[][] grid;

	public FourInARowModel() {
		this.grid = new Cell[GRID_WIDTH][GRID_WIDTH];
		initializeGrid();
	}

	public void initializeGrid() {
		int xx = FRAME_THICKNESS;
		for (int x = 0; x < GRID_WIDTH; x++) {
			int yy = FRAME_THICKNESS;
			for (int y = 0; y < GRID_WIDTH; y++) {
				Cell cell = new Cell(0);
				cell.setCellLocation(xx, yy);
				grid[x][y] = cell;
				yy += FRAME_THICKNESS + Cell.getCellWidth();
			}
			xx += FRAME_THICKNESS + Cell.getCellWidth();
		}
	}

	public Cell getCell(int x, int y) {
		return grid[x][y];
	}

	public boolean isGameOver() {
		return isGridFull();
	}

	private boolean isGridFull() {
		for (int x = 0; x < GRID_WIDTH; x++) {
			for (int y = 0; y < GRID_WIDTH; y++) {
				if (grid[x][y].isZeroValue()) {
					return false;
				}
			}
		}
		return true;
	}

	public void addNewCell(int x, int y, int value) {

		if (grid[x][y].isZeroValue()) {
			grid[x][y].setValue(value);
			/*
			 * if (DEBUG) { System.out.println(displayAddCell(x, y)); }
			 */
		}
	}

	public Dimension getPreferredSize() {
		int width = GRID_WIDTH * Cell.getCellWidth() + FRAME_THICKNESS * 5;
		return new Dimension(width, width);
	}

	public void draw(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		Dimension d = getPreferredSize();
		g.fillRect(0, 0, d.width, d.height);

		for (int x = 0; x < GRID_WIDTH; x++) {
			for (int y = 0; y < GRID_WIDTH; y++) {
				grid[x][y].draw(g);
			}
		}
	}
}
