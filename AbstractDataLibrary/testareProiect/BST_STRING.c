#include "testerHeader.h"
#define MAX_STRING 20
#define MAX_KEY 2
void BST_String(int argc, char** argv, int indice)//indice = al catelea fisier din argv
{
    fisiere f;
    determineFiles(argc,argv,indice,&f);

    char comanda[30];
    char c, k;
    BST *b, *bb;

    VectorStructures vs[10];
    int sizevs = 0;

    while(!feof(f.f))
    {
        fscanf(f.f,"%s %c ",comanda,&c);

        if(!strcmp(comanda, "CreateBST"))
        {
            b = CreateBST(printString, cmpStrings);
            vs[sizevs].structura = b;
            vs[sizevs].denumire = c;
            sizevs++;
        }
        else if(!strcmp(comanda, "AddBSTItem"))
        {
            void *data;
            void *key;

            data = (char*)malloc(sizeof(void*) * MAX_STRING);
            key = (char*)malloc(sizeof(void*) * MAX_KEY);
            fscanf(f.f,"%s %s ",(char*)data, (char*)key);
            *((char*)key+1)='\0';

            b = (BST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                AddBSTItem(b, key, data);
            }
        }
        else if(!strcmp(comanda, "PreorderBST"))
        {
            b = (BST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PreorderBST(b, f.g);
            }
        }
        else if(!strcmp(comanda, "PrintBST"))
        {
            b = (BST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PrintBST(b, f.g);
            }
        }

        else if(!strcmp(comanda, "InorderBST"))
        {
            b = (BST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                InorderBST(b, f.g);
            }
        }
        else if(!strcmp(comanda, "PostorderBST"))
        {
            b = (BST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PostorderBST(b, f.g);
            }
        }
        else if(!strcmp(comanda, "HeightBST"))
        {
            b = (BST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
                fprintf(f.g, "%d\n", HeightBST(b));
        }
        else if(!strcmp(comanda, "DeleteBSTItem"))
        {
            void* key = malloc(sizeof(void*) * MAX_KEY);
            fscanf(f.f,"%s ",(char*)key);

            b = (BST*)determinaStructura(vs,sizevs, c);
            if(b != NULL)
                DeleteBSTItem(b,key);
        }
        else if(!strcmp(comanda, "SearchBSTItem"))
        {
            void* key = malloc(sizeof(void*) * MAX_KEY);
            nodeBST *n;

            fscanf(f.f,"%s ",(char*)key);

            b = (BST*)determinaStructura(vs,sizevs, c);
            if(b != NULL)
            {
                n = SearchBSTItem(b,key);
                if(n != NULL)
                {
                    fprintf(f.g,"%s \n", (char*)n->data);
                }
                else
                    fprintf(f.g, "Elementul nu a fost gasit!\n");
            }
        }
        else if(!strcmp(comanda, "DeleteBST"))
        {
            b = (BST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                DeleteBST(b);
                for(int i=0; i < sizevs; i++)
                {
                    if(vs[i].denumire == c)
                    {
                        vs[i].structura = NULL;
                        break;
                    }
                }
            }
        }
        else if(!strcmp(comanda, "MergeBSTs"))
        {
            fscanf(f.f,"%c ",&k);
            b = (BST*)determinaStructura(vs, sizevs, c);
            bb = (BST*)determinaStructura(vs, sizevs, k);
            if(b != NULL && bb != NULL)
            {
                MergeBSTs(b, bb);
            }
        }
    }
    fclose(f.f);
    fclose(f.g);
}
