package PT2017.demo.Polinoame;

import org.junit.Test;

import junit.framework.TestCase;

public class MainTest extends TestCase {

	private static Model model = new Model();
		
	@Test
	public void test1Add(){
		Polinom test = new Polinom("12x^4-6x^3+33x^2+x+10+15x^-1");
		model.createPolinom1("12x^4-6x^3+30x^2-x+8+15x^-1");
		model.createPolinom2("3x^2+2x+2");		
		assert(model.addPolinoame(model.getPolinom1(), model.getPolinom2()).toString().equals(test.toString()));
	}
	
	@Test
	public void test1Sub(){
		Polinom test = new Polinom("12x^4-6x^3+27x^2-3x+6+15x^-1");
		model.createPolinom1("12x^4-6x^3+30x^2-x+8+15x^-1");
		model.createPolinom2("3x^2+2x+2");
		assert(model.subPolinoame(model.getPolinom1(), model.getPolinom2()).toString().equals(test.toString()));
	}
	
	@Test
	public void test1Mul(){
		Polinom test = new Polinom("36x^6-18x^5+24x^4-6x^3+18x^2+4x^1+12x^0");
		model.createPolinom1("12x^4-6x^3+2x^1+6x^0");
		model.createPolinom2("3x^2+2x^0");
		assert(model.multiplyPolinoame(model.getPolinom1(), model.getPolinom2()).toString().equals(test.toString()));
	}
	
	@Test
	public void test1Div(){
		String test = new String("4x^2-2x-2.6666667 rest: 6x+12.333334");
		Polinom rezultat = new Polinom();
		model.createPolinom1("12x^4-6x^3+2x^1+7");
		model.createPolinom2("3x^2+2");
		try {
			rezultat = model.dividePolinoame(model.getPolinom1(), model.getPolinom2());
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assert((rezultat.toString() + " rest: " + model.getRest().toString()).equals(test));
	}
	
	@Test
	public void test2Div(){
		String test = new String("4x^2-2x-2 rest: -2x^2+6x+11");
		Polinom rezultat = new Polinom();
		model.createPolinom1("12x^4-6x^3+2x^1+7");
		model.createPolinom2("3x^2+2");
		try {
			rezultat = model.dividePolinoameIntreg(model.getPolinom1(), model.getPolinom2());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		assert((rezultat.toString() + " rest: " + model.getRest().toString()).equals(test));
	}
	
	@Test
	public void test3Div(){
		model.createPolinom1("12x^4-6x^3+2x^1+7");
		model.createPolinom2("0");
		try {
			model.dividePolinoame(model.getPolinom1(), model.getPolinom2());
			fail();
		}
		catch (ArithmeticException e) {
			//e.printStackTrace();
			assertTrue(true);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			assertTrue(true);
		}
	}
	
	@Test
	public void test4Div(){
		model.createPolinom1("12x^4-6x^3+2x^1+7");
		model.createPolinom2("0");
		try {
			model.dividePolinoameIntreg(model.getPolinom1(), model.getPolinom2());
			fail();
		}
		catch (ArithmeticException e) {
			//e.printStackTrace();
			assertTrue(true);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			assertTrue(true);
		}
	}
	
	@Test
	public void test1Integ(){
		String test = new String("2.4x^5-1.5x^4+x^2+6x");
		model.createPolinom1("12x^4-6x^3+2x^1+6x^0");
		assert(model.integratePolinom(model.getPolinom1()).toString().equals(test));
	}
	
	@Test
	public void test1Der(){
		Polinom test = new Polinom("48x^3-18x^2+2x^0");
		model.createPolinom1("12x^4-6x^3+2x^1+6x^0");
		assert(model.derivatePolinom(model.getPolinom1()).toString().equals(test.toString()));
	}
}