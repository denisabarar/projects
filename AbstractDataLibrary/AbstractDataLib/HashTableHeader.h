#ifndef HASHTABLEHEADER_H_INCLUDED
#define HASHTABLEHEADER_H_INCLUDED


typedef struct Element {
    unsigned long hash;
	void *key;
	void *value;
	struct Element *next;
}Element;

typedef struct HashTable{
	int tableSize;
	int nrElements;
	Element **table;

	void (*print) (const void*, FILE*);
	int (*cmp) (const void*, const void*);
	int (*hashFunction)(const void*);
}HashTable;

HashTable* CreateHashTable(int capacity, int (*hashFunction)(const void*), void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*));
void PrintHashTable(HashTable *hT, FILE* f);
void AddHashTableItem(HashTable *hT, void *key, void *value);
void DeleteHashTableItem(HashTable *hT, const void *key, const void *value);
int SearchHashTableItem(HashTable *hT, const void *key, const void *value);
HashTable* ReHashTable(HashTable *hT, int (*hashFunction)(const void*));
void DeleteHashTable(HashTable *hT);

Element* CreateElement(void *key, void *value);
struct Element* GetHashTableElement(HashTable *hT, const void *key);

void ResizeHashTable(HashTable *hT);
size_t hashImplicit (const char *s, size_t m);
int hashFunctionString (const char *s);
int hashFunctionInt (const void *x);
int hashFunctionInt2(const void *x);


#endif // HASHTABLEHEADER_H_INCLUDED
/*
a. CreateHashTable - creare instanță HashTable
i. Se transmite ca parametru functia de dispersie
1. Dacă e NULL se folosește o funcție implicită
b. PrintHashTable – afișarea elementelor astfel încât să reflecte structura tabelei
c. AddHashTableItem – adăugarea unui nou element
i. Dacă factorul de umplere depășește 75% se face redimensionare (aprox. dublare)
d. DeleteHashTableItem – ștergerea elementului cu valoarea specificată
e. SearchHashTableItem – returnează dacă valoarea căutată este în tabela de dispersie
f. ReHashTable – crearea unei noi tabele de hashing și redistribuirea elementelor folosind o funcție de hashing specificată,
urmată de ștergerea tabelei inițiale
g. DeleteHashTable – ștergerea elementelor și eliberarea memoriei*/
