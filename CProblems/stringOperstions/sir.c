#include "header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* citire_sir()
{
    fflush(stdin);
    char *s=(char*)malloc(MAX_S);
    printf("dati un sir: \n");
    scanf("%[^'\n']s",s);
    return s;
}
void afisare_sir(char *s)
{
   printf("sirul este: %s\n",s);
}
void eliminare_spatii_albe(char *s)
{
   char *ss=s;
   do
   {
      while((*s)==' ')
         s++;
   }
   while(*ss++ = *s++);
}

void eliminare_ultim_cuvant(char *s)
{
    char *c;
    for(char *p=s;p<s+strlen(s);p++)
        if(*p==' ')
            c=p;
    if(*c!=NULL)
        *c='\0';
}

void eliminare_aparitie_subsir(char* s,char* subs)
{
    char *p;
    p=strstr(s,subs);
    while(p)
    {
        if((p==s || *(p-1)==' ') && *(p+strlen(subs))==' ')
            strcpy(p,p+strlen(subs)+1);
        else
            p++;
        p=strstr(p,subs);
    }
}

void inserare_pozitie(char *s, int poz, char *subs)
{
    char aux[MAX_S];
    int cantitate=poz;
    strcpy(aux,s+cantitate);
    if(*(s+poz-1)!=' ')
    {
        strcpy(s+poz," ");
        cantitate++;
    }
    strcpy(s+cantitate,subs);
    cantitate+=strlen(subs);
    if(*(aux)!=' ')
    {
        strcpy(s+cantitate," ");
        cantitate++;
    }
    strcpy(s+cantitate,aux);
    *(s+cantitate+strlen(aux))='\0';
}

void initializare_vector(int n,int v[n])
{
    for(int i=0;i<n;i++)
        v[i]=0;
}
void afisare_vector_histograma(int n, int v[n])
{
    for(int i=0;i<n;i++)
        if(v[i]>0)
            printf("caracterul %c a aparut de %d ori\n",i,v[i]);
}
void histograma_litere(char* s, int *v)
{
    initializare_vector(MAX_V,v);
    for(char *p=s;p<s+strlen(s);p++)
    {
        v[*p]++;
    }
    afisare_vector_histograma(MAX_V,v);
}

