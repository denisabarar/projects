#include "AbstractData.h"
#include <math.h>

#define LCHILD(X) (2 * (X) + 1)
#define RCHILD(X) (2 * (X) + 2)
#define PARENT(X) (((X) -1) / 2)


struct MinHeap_s
{
    int capacity;
    int size;
    void **vector;
    void (*print) (const void*, FILE*);
    int (*cmp) (const void*, const void*);
};

MinHeap* CreateHeap(int capacity, void (*print) (const void*, FILE*),int (*cmp) (const void*, const void*))
{
    MinHeap *h = malloc(sizeof(MinHeap));
    if(h == NULL)
    {
        fprintf(stderr,"Nu s-a alocat memorie pentru heap");
        return NULL;
    }
    h->size = 0;
    h->capacity = capacity;

    void **a =(void*) malloc(h->capacity * sizeof(void*));
    if(a == NULL)
    {
        free(h);
        fprintf(stderr,"Nu s-a alocat memorie pentru vector");
        return NULL;
    }
    h->vector = a;
    h->print = print;
    h->cmp = cmp;
    return h;
}

void PrintHeap(MinHeap *h, FILE *f)
{
    //preorderTraversal(h, 0, f,0);
    //inorderTraversal(h ,0 ,f);
    levelorderTraversal(h,f);

    fprintf(f,"\n");
    int max = 0;
    for(int i = 0; i < h->size; i++)
    {
        for(int j = 0; j < pow(2, i) && j + pow(2, i) < h->size; j++)
        {
            if(j > max)
                max = j;
        }
    }
    for(int i = 0; i < h->size; i++)
    {
          for(int j = 0; j < pow(2, i) && j + pow(2, i) - 1< h->size; j++)
          {
              for(int k = 0; (k < max / ((int)pow(2, i))); k++)
              {
                  fprintf(f, "  ");
              }
              h->print(h->vector[j + (int)pow(2, i) - 1], f);
          }
          fprintf(f,"\n");
    }
    fprintf(f,"\n");
}

void inorderTraversal(MinHeap *h, int i, FILE *f)
{
    for(int j = 0; j <= i; j++)
        fprintf(f," ");

    if(LCHILD(i) < h->size)
    {
        inorderTraversal(h, LCHILD(i), f);
    }

    h->print(h->vector[i],f);

    if(RCHILD(i) < h->size)
    {
        inorderTraversal(h, RCHILD(i), f);
    }
    fprintf(f,"\n");
}

void preorderTraversal(MinHeap *h, int i, FILE *f, int level)
{
    for(int j = 0; j <= level; j++)
        fprintf(f," ");
    h->print(h->vector[i],f);

    if(LCHILD(i) < h->size)
    {
        preorderTraversal(h, LCHILD(i), f, level+1) ;
    }
    if(RCHILD(i) < h->size)
    {
        preorderTraversal(h, RCHILD(i), f, level+1) ;
    }
    fprintf(f,"\n");

}

void levelorderTraversal(MinHeap *h, FILE *f)
{
    for(int i = 0;i < h->size; i++)
    {
          h->print(h->vector[i],f);
    }
}
#define MULTIPLIER 2
void AddHeapItem(MinHeap *h, void *data)
{
    int i;

    if(h->size >= h->capacity)
    {
        h->capacity = h->capacity * MULTIPLIER;
        realloc(h->vector, h->capacity * sizeof(MinHeap));
    }

    i = (h->size)++;

    while(i && h->cmp(data, h->vector[PARENT(i)]) < 0)
    {
        h->vector[i] = h->vector[PARENT(i)];
        i = PARENT(i);
    }
    h->vector[i] = data;
}

void* GetHeapMin(MinHeap *h)
{
    return h->vector[0];
}

void MinHeapify(MinHeap *h, int i)
{
    int lowest;
    int l = LCHILD(i);
    int r = RCHILD(i);

    if(l < h->size && h->cmp(h->vector[l], h->vector[i]) < 0)
    {
        lowest = l;
    }
    else
    {
        lowest = i;
    }
    if(r < h->size && h->cmp(h->vector[r], h->vector[lowest]) < 0)
    {
        lowest = r;
    }

    if(lowest != i)
    {
        void *temp = h->vector[i];
        h->vector[i] = h->vector[lowest];
        h->vector[lowest] = temp;
        MinHeapify(h, lowest);
    }
}

void* DeleteHeapMin(MinHeap *h)
{
    if(h->size == 0)
    {
        fprintf(stderr, "MinHeap este vid");
        return NULL;
    }

    void *data = h->vector[0];
    (h->size)--;
    h->vector[0] = h->vector[h->size];
    //h->vector = realloc(h->vector, h->capacity * sizeof(node));
    MinHeapify(h, 0);
    return data;
}

void DeleteHeapItem(MinHeap *h, void *data)
{
    if(h->size == 0)
    {
        fprintf(stderr, "MinHeap este vid");
        return ;
    }

    int index = SearchHeapIndex(h, data);
    if(index != -1)
    {
        (h->size)--;
        h->vector[index] = h->vector[h->size];
        MinHeapify(h,index);
    }
}

void DeleteHeap(MinHeap *h)
{
     if(h == NULL)
    {
        fprintf(stderr, "Heap inexistent, nu se poate sterge!\n");
    }
    else
    {
        h->size = 0;
        free(h->vector);
        free(h);
    }
}

int SearchHeapIndex(MinHeap *h, void *data)
{
    for(int i = 0;i < h->size; i++)
    {
        if(h->cmp(h->vector[i], data) == 0)
            return i;
    }
    return -1;
}


void MergeMinHeaps(MinHeap *h, MinHeap *hh)
{
    int i;
    void *data;

    if(h->size + hh->size >= h->capacity)
    {
        h->capacity = h->capacity * MULTIPLIER;
        realloc(h->vector, h->capacity * sizeof(MinHeap));
    }

    for(int j = 0; j < hh->size; j++)
    {
        data = hh->vector[j];
        i = (h->size)++;

        while(i && h->cmp(data, h->vector[PARENT(i)]) < 0)
        {
            h->vector[i] = h->vector[PARENT(i)];
            i = PARENT(i);
        }
        h->vector[i] = data;
    }
}
