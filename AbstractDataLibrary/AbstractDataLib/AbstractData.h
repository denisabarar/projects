#ifndef ABSTRACTDATA_H_INCLUDED
#define ABSTRACTDATA_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "VectorHeader.h"
#include "LinkedListHeader.h"
#include "HashTableHeader.h"
#include "MinHeap.h"
//#include "BST.h"
//#include "BalancedBST.h"
#include "BST2.h"
#include "BalancedBST2.h"

int cmpNumbers(const void* i1, const void* i2);
int cmpStrings(const void* i1, const void* i2);

void printInt(const void* i, FILE *f);
void printChar(const void* i, FILE *f);
void printFloat(const void* i, FILE *f);
void printString(const void *i, FILE *f);

int indexVerification(int index, int size);

#endif // ABSTRACTDATA_H_INCLUDED
