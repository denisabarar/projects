import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseClickListener implements MouseListener {

	private FourInARowFrame frame;
	private FourInARowModel model;

	public MouseClickListener(FourInARowFrame frame, FourInARowModel model) {
		this.frame = frame;
		this.model = model;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (model.getMouseActive()) {
			if (e.getX() < this.model.getWidth()) {
				model.addCell(e.getX() / (this.model.getWidth() / 8));
				frame.updatePlayer();
				frame.repaintGridPanel();
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
