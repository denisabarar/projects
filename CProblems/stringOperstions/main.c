#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"
/*eliminarea spațiilor albe în exces dintr-un șir de caractere
 eliminarea ultimei apariții a unui cuvânt dintr-un șir de caractere
 eliminarea fiecărei apariții a unui cuvânt dintr-un șir de caractere
 inserarea unui cuvânt la o poziție dată în șirul de caractere
 determinarea histogramei de apariție a caracterelor dintr-un șir de caractere
creați un modul SIRURI care să cuprindă funcțiile de mai sus și alte funcționalități utile în lucrul cu șiruri de caractere*/

int main()
{
    char *s=(char*)malloc(MAX_S);
    char *subs=(char*)malloc(MAX_S);
    int v[MAX_V];
    int m;
    int poz;

    do
    {
        printf("\ndati meniul:\n1:el spatii albe\n2:el ultima aparitie\n3:el fiecare aparitie\n4: inser pe poz\n5:histograma\n\n");
        scanf("%d",&m);
        fflush(stdin);
        switch(m)
        {
        case 1:
            s=citire_sir();
            eliminare_spatii_albe(s);
            afisare_sir(s);
            break;
        case 2:
            s=citire_sir();
            eliminare_ultim_cuvant(s);
            afisare_sir(s);
            break;
        case 3:
            s=citire_sir();
            printf("pentru subsir: \n");
            subs=citire_sir();
            eliminare_aparitie_subsir(s,subs);
            afisare_sir(s);
            break;
        case 4:
            s=citire_sir();
            printf("dati pozitie:\n");
            scanf("%d",&poz);
            printf("pentru subsir, ");
            subs=citire_sir();
            inserare_pozitie(s,poz,subs);
            afisare_sir(s);
            break;
        case 5:
            s=citire_sir();
            histograma_litere(s,v);
            break;
        }
    }
    while(m!=0);
    return 0;
}
