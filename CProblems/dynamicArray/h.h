#include <stdio.h>
#include <stdlib.h>
#ifndef H_H_INCLUDED
#define H_H_INCLUDED
#define MAX 100
int n,m;
int **aloca_matrice(int n,int m);
void citire_matrice(int **v,int n,int m);
void adauga_linie(int ***v,int *n,int m,int linie);
void adauga_coloana(int ***v,int n,int *m,int coloana);
void adauga_inel(int ***v,int *n,int *m);
void afisare_matrice(int **v,int n,int m);
void dealoca_matrice(int **v,int n,int m);
#endif // H_H_INCLUDED
