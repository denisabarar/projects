#include "testerHeader.h"

void BalancedBST_Int2(int argc, char** argv, int indice)//indice = al catelea fisier din argv
{
    fisiere f;
    determineFiles(argc,argv,indice,&f);

    char comanda[30];
    char c, k;
    BalancedBST *b, *bb;

    VectorStructures vs[10];
    int sizevs = 0;

    while(!feof(f.f))
    {
        fscanf(f.f,"%s %c ",comanda,&c);

        if(!strcmp(comanda, "CreateBalancedBST"))
        {
            b = CreateBalancedBST(printInt,cmpNumbers);
            vs[sizevs].structura = b;
            vs[sizevs].denumire = c;
            sizevs++;
        }
        else if(!strcmp(comanda, "AddBalancedBSTItem"))
        {
            void *data;
            void *key;

            data = (int*)malloc(sizeof(void*));
            key = (int*)malloc(sizeof(void*));
            fscanf(f.f,"%d ",(int*)data);
            *((int*)key) = *((int*)data);

            b = (BalancedBST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                AddBalancedBSTItem(b, key, data);
            }
        }
        else if(!strcmp(comanda, "PreorderBalancedBST"))
        {
            b = (BalancedBST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PreorderBalancedBST(b, f.g);
            }
        }
        else if(!strcmp(comanda, "PrintBalancedBST"))
        {
            b = (BalancedBST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PrintBalancedBST(b, f.g);
            }
        }

        else if(!strcmp(comanda, "InorderBalancedBST"))
        {
            b = (BalancedBST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                InorderBalancedBST(b, f.g);
            }
        }
        else if(!strcmp(comanda, "PostorderBalancedBST"))
        {
            b = (BalancedBST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PostorderBalancedBST(b, f.g);
            }
        }
        else if(!strcmp(comanda, "HeightBalancedBST"))
        {
            b = (BalancedBST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
                fprintf(f.g, "%d\n", HeightBalancedBST(b));
        }
        else if(!strcmp(comanda, "DeleteBalancedBSTItem"))
        {
            void* key = (int*)malloc(sizeof(int));

            fscanf(f.f,"%d ",(int*)key);

            b = (BalancedBST*)determinaStructura(vs,sizevs, c);
            if(b != NULL)
                DeleteBalancedBSTItem(b,key);
        }
        else if(!strcmp(comanda, "SearchBalancedBSTItem"))
        {
            void* key = (int*)malloc(sizeof(int));
            nodeAVL* n;

            fscanf(f.f,"%d ",(int*)key);

            b = (BalancedBST*)determinaStructura(vs,sizevs, c);
            if(b != NULL)
            {
                n = SearchBalancedBSTItem(b,key);
                if(n != NULL)
                {
                    fprintf(f.g,"%d \n",*((int*)n->data));
                }
                else
                    fprintf(f.g, "Elementul nu a fost gasit!\n");
            }
        }
        else if(!strcmp(comanda, "DeleteBalancedBST"))
        {
            b = (BalancedBST*)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                DeleteBalancedBST(b);
                for(int i=0; i < sizevs; i++)
                {
                    if(vs[i].denumire == c)
                    {
                        vs[i].structura = NULL;
                        break;
                    }
                }
            }
        }
        else if(!strcmp(comanda, "MergeBalancedBSTs"))
        {
            fscanf(f.f,"%c ",&k);
            b = (BalancedBST*)determinaStructura(vs, sizevs, c);
            bb = (BalancedBST*)determinaStructura(vs, sizevs, k);
            if(b != NULL && bb != NULL)
            {
                MergeBalancedBSTs(b, bb);
            }
        }
    }
    fclose(f.f);
    fclose(f.g);
}
