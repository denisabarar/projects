#include <stdio.h>
#include <stdlib.h>
/*Scrieţi un program C care citește elementele unei matrice NxN alocată dinamic și apoi, folosind doar pointeri și expresii
cu pointeri afișează
a. sumele elementelor pe linii
b. sumele elementelor pe coloane
c. suma elementelor de pe diagonala principală
d. suma elementelor de pe diagonala secundară
e. sumele elementelor de pe inele de la exterior spre interior.
OBS: care este diferența dintre alocarea unei matrice ca un vector de N*M elemente respectiv ca un vector de N
vectori de câte M elemente?*/
#define N 5
int** aloca_matrice()
{
    int **v=(int**)malloc(N*sizeof(int*));
    for(int i=0;i<N;i++)
        v[i]=(int*)malloc(N*sizeof(int));
    return v;
}

void citeste_matrice(int**v)
{
    int k=1;
    for(int i=0;i<N;i++)
        for(int j=0;j<N;j++)
    {
        //printf("v[%d][%d]=",i,j);
        //scanf("%d",&v[i][j]);
        v[i][j]=k++;
    }
}

void afisare(int **v)
{
    for(int i=0;i<N;i++)
    {
        for(int j=0;j<N;j++)
            printf("%4d",v[i][j]);
        printf("\n");
    }
}

void suma_linii(int **v)
{
    int suma=0;
    for(int i=0;i<N;i++)
    {
        suma=0;
        for(int j=0;j<N;j++)
            suma+=v[i][j];
        printf("suma linia %d: %d\n",i,suma);
    }
}

void suma_coloane(int **v)
{
    int suma=0;
    for(int i=0;i<N;i++)
    {
        suma=0;
        for(int j=0;j<N;j++)
            suma+=v[j][i];
        printf("suma coloana %d: %d\n",i,suma);
    }
}

void suma_diag_pr(int **v)
{
    int suma=0;
    for(int i=0,j=0;i<N;i++,j++)
        suma+=v[i][j];
    printf("suma diag pr: %d\n",suma);
}

void suma_diag_sec(int **v)
{
    int suma=0;
    for(int i=0,j=N-i-1;i<N;i++,j--)
        suma+=v[i][j];
    printf("suma diag sec: %d\n",suma);
}

void suma_inele(int **v)
{
    int suma;
    int elem;
    for(int p=0;p<N/2;p++)
    {
        suma=0;
        for(int i=p;i<N-p;i++)
        {
                elem=v[i][p];
            suma+=v[i][p];
                elem=v[i][N-p-1];
            suma+=v[i][N-p-1];
                elem=v[p][i];
            suma+=v[p][i];
                elem=v[N-p-1][i];
            suma+=v[N-p-1][i];
        }
        suma-=v[p][p]+v[N-p-1][N-p-1]+v[p][N-p-1]+v[N-p-1][p];
        printf("suma inelul %d: %d\n",p,suma);
    }
    if(N%2)
        printf("suma inelul %d: %d\n",N/2,v[N/2][N/2]);
}

int main()
{
    int **v=aloca_matrice();
    citeste_matrice(v);
    afisare(v);
    suma_linii(v);
    suma_coloane(v);
    suma_diag_pr(v);
    suma_diag_sec(v);
    suma_inele(v);
    return 0;
}
