#include "testerHeader.h"

void determineFiles(int argc, char** argv, int indice, fisiere *f)
{
    char *numeFisierIn = (char*)malloc(strlen(argv[indice]));
    char *numeFisierOut = (char*)malloc(strlen(argv[indice]+1));

    strcpy(numeFisierIn, argv[indice]);
    sscanf(numeFisierIn,"%[^.]s",numeFisierOut);
    strcat(numeFisierOut,".out");

    f->f=fopen(numeFisierIn, "r");
    if(!(f->f))
        exit(1);
    f->g=fopen(numeFisierOut,"w");
    if(!(f->g))
        exit(1);
}
