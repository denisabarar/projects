#ifndef MINHEAP_H_INCLUDED
#define MINHEAP_H_INCLUDED


typedef struct MinHeap_s MinHeap;
typedef struct Node_s Node;

MinHeap* CreateHeap(int capacity, void (*print) (const void*, FILE*),int (*cmp) (const void*, const void*));
void PrintHeap(MinHeap *h, FILE *f);
void AddHeapItem(MinHeap *h, void *data);
void* GetHeapMin(MinHeap *h);
void* DeleteHeapMin(MinHeap *h);
void DeleteHeapItem(MinHeap *h, void *data);
void DeleteHeap(MinHeap *h);
void MergeMinHeaps(MinHeap *h, MinHeap *hh);

int SearchHeapIndex(MinHeap *h, void *data);
void MinHeapify(MinHeap *h, int i);
void inorderTraversal(MinHeap *h, int i, FILE *f);
void preorderTraversal(MinHeap *h, int i, FILE *f,int level);
void levelorderTraversal(MinHeap *h, FILE *f);
#endif // MINHEAP_H_INCLUDED
/*
MinHeap (implementat ca un tablou alocat dinamic)
Operații
a. CreateHeap - creare instanță Heap
b. PrintHeap – afișarea elementelor sub formă de arbore
c. AddHeapItem – adăugarea unui nou element
d. GetHeapMin – returnează elementul minim
e. DeleteHeapMin – ștergerea și returnarea elementului minim
f. DeleteHeapItem – ștergerea elementului cu valoarea specificată
g. DeleteHeap – ștergerea elementelor și eliberarea memoriei*/
