
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class FourInARowModel extends JPanel {

	private static final int FRAME_THICKNESS = 16;
	private static final int GRID_WIDTH = 8;

	private static final long serialVersionUID = 4019841629547494495L;
	private GameOverImage gameOverImage;
	private WinnerImageRED winnerImageRED;
	private WinnerImage winnerImageBLUE;

	static Cell[][] grid = new Cell[8][8];

	static Cell.Player player;
	static Cell.Player winner = Cell.Player.NONE;

	private boolean mouseActive = false;

	public FourInARowModel() {
		this.gameOverImage = new GameOverImage(this);
		this.gameOverImage.run();

		this.winnerImageRED = new WinnerImageRED(this);
		this.winnerImageRED.run();

		this.winnerImageBLUE = new WinnerImage(this);
		this.winnerImageBLUE.run();

		initializeGrid();
		initializePlayer();
	}

	public void initializeGrid() {
		int xx = FRAME_THICKNESS;
		for (int x = 0; x < GRID_WIDTH; x++) {
			int yy = FRAME_THICKNESS;
			for (int y = 0; y < GRID_WIDTH; y++) {
				Cell cell = new Cell(Cell.Player.NONE);
				cell.setCellLocation(xx, yy);
				grid[x][y] = cell;
				yy += FRAME_THICKNESS + Cell.getCellWidth();
			}
			xx += FRAME_THICKNESS + Cell.getCellWidth();
		}
	}

	public void initializePlayer() {
		player = Cell.Player.RED;
		winner = Cell.Player.NONE;
	}

	public Cell.Player getPlayer() {
		return player;
	}

	public Cell.Player alternatePlayer(Cell.Player player) {
		return (player == Cell.Player.RED) ? Cell.Player.BLUE : Cell.Player.RED;
	}

	public void addCell(int y) {
		if (!isGameOver()) {
			if (!isGridFull()) {
				if (mouseActive == true) {
					for (int i = GRID_WIDTH - 1; i >= 0; i--) {
						if (grid[y][i].isZeroValue()) {
							grid[y][i].setValue(player);
							player = alternatePlayer(player);
							break;
						}
					}
				}
			}
		}
	}

	public Cell.Player isWinner() {
		/*
		 * if(isWinnerOrizontally() != Cell.Player.NONE ){ mouseActive = false;
		 * return isWinnerOrizontally(); } if(isWinnerVertically() !=
		 * Cell.Player.NONE){ mouseActive = false; return isWinnerVertically();
		 * } return Cell.Player.NONE;
		 */

		// check for a horizontal win
		for (int row = 0; row < GRID_WIDTH; row++) {
			for (int col = 0; col < GRID_WIDTH - 3; col++) {
				if (grid[col][row].getValue() != Cell.Player.NONE
						&& grid[col][row].getValue() == grid[col + 1][row].getValue()
						&& grid[col][row].getValue() == grid[col + 2][row].getValue()
						&& grid[col][row].getValue() == grid[col + 3][row].getValue()) {
					winner = grid[col][row].getValue();
				}
			}
		}
		// check for a vertical win
		for (int row = 0; row < GRID_WIDTH - 3; row++) {
			for (int col = 0; col < GRID_WIDTH; col++) {
				if (grid[col][row].getValue() != Cell.Player.NONE && grid[col][row].getValue() == grid[col][row + 1].getValue()
						&& grid[col][row].getValue() == grid[col][row + 2].getValue() && grid[col][row].getValue() == grid[col][row + 3].getValue()) {
					winner = grid[col][row].getValue();
				}
			}
		}
		// check for a diagonal win (positive slope)
		for (int row = 0; row < GRID_WIDTH - 3; row++) {
			for (int col = 0; col < GRID_WIDTH - 3; col++) {
				if (grid[col][row].getValue() != Cell.Player.NONE && grid[col][row].getValue() == grid[col + 1][row + 1].getValue()
						&& grid[col][row].getValue() == grid[col + 2][row + 2].getValue() && grid[col][row].getValue() == grid[col + 3][row + 3].getValue()) {
					winner = grid[col][row].getValue();
				}
			}
		}
		// check for a diagonal win (negative slope)
		for (int row = 3; row < GRID_WIDTH; row++) {
			for (int col = 0; col < GRID_WIDTH - 3; col++) {
				if (grid[col][row].getValue() != Cell.Player.NONE && grid[col][row].getValue() == grid[col + 1][row - 1].getValue()
						&& grid[col][row].getValue() == grid[col + 2][row - 2].getValue() && grid[col][row].getValue() == grid[col + 3][row - 3].getValue()) {
					winner = grid[col][row].getValue();
				}
			}
		}
		return winner;
	}

	public Cell getCell(int x, int y) {
		return grid[x][y];
	}

	public boolean isGameOver() {
		return isGridFull();
	}

	private boolean isGridFull() {
		for (int x = 0; x < GRID_WIDTH; x++) {
			for (int y = 0; y < GRID_WIDTH; y++) {
				if (grid[x][y].isZeroValue()) {
					return false;
				}
			}
		}
		return true;
	}

	public void setMouseActive(boolean mouseActive) {
		this.mouseActive = mouseActive;
	}

	public boolean getMouseActive() {
		return mouseActive;
	}

	public Dimension getPreferredSize() {
		int width = GRID_WIDTH * Cell.getCellWidth() + FRAME_THICKNESS * 9;
		return new Dimension(width, width);
	}

	public void draw(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		Dimension d = getPreferredSize();
		g.fillRect(0, 0, d.width, d.height);

		for (int x = 0; x < GRID_WIDTH; x++) {
			for (int y = 0; y < GRID_WIDTH; y++) {
				{
					grid[x][y].draw(g);
				}
			}
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.draw(g);

		if (this.isGameOver()) {
			g.drawImage(gameOverImage.getImage(), 0, 0, null);
			this.mouseActive = false;
		}

		if (this.isWinner() == Cell.Player.RED) {
			g.drawImage(winnerImageRED.getImage(), 0, 0, null);
			this.mouseActive = false;
		}

		if (this.isWinner() == Cell.Player.BLUE) {
			g.drawImage(winnerImageBLUE.getImage(), 0, 0, null);
			this.mouseActive = false;
		}
	}

	public int getSizeCell() {
		return GRID_WIDTH;
	}

	public int getWidth() {
		return GRID_WIDTH * Cell.getCellWidth() + FRAME_THICKNESS * 9;
	}
}
