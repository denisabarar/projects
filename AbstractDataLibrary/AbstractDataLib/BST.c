#include "AbstractData.h"
/*

nodeBST* CreateBST()
{
    nodeBST *root = NULL;

    return root;
}

nodeBST* CreateNodeBst(void *key, void *data)
{
    nodeBST* n = malloc(sizeof(nodeBST));
    n->key = key;
    n->data = data;
    n->left = NULL;
    n->right = NULL;

    return n;
}

void PrintBST(nodeBST* root, FILE *f, void (*print) (const void*, FILE*),int level)
{
    if(root == NULL)
    {
        return ;
    }

    PrintBST(root->left, f, print, level + 1);
    for(int i = 0; i <= level; i++)
        fprintf(f, "  ");
    print(root->data, f);
    fprintf(f, "\n");
    PrintBST(root->right, f, print, level + 1);
    //int h = HeightBST(b);
    //for(int i = 1; i <= h; i++)
    //{
    //    PrintGivenLevel(root, f, print, i);
    //    fprintf(f, "\n");
    //}
}

void PrintGivenLevel(nodeBST *root, FILE *f, void (*print) (const void*, FILE*), int level)
{
    if (root != NULL)
    {
        for(int i = 0; i <= level; i++)
            fprintf(f, " ");

        if (level == 1)
        {
            print(root->data, f);
        }
        else if (level > 1)
        {
            PrintGivenLevel(root->left, f, print, level-1);
            PrintGivenLevel(root->right, f, print, level-1);
        }
    }
}

void PreorderBST(nodeBST *root, FILE *f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    print(root->data, f);
    PreorderBST(root->left, f, print);
    PreorderBST(root->right, f, print);
}

void InorderBST(nodeBST *root, FILE *f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    InorderBST(root->left, f, print);
    print(root->data, f);
    InorderBST(root->right, f, print);
}

void PostorderBST(nodeBST *root, FILE *f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    PostorderBST(root->left, f, print);
    PostorderBST(root->right, f, print);
    print(root->data, f);
}

nodeBST* AddBSTItem(nodeBST *root, void *key, void *data, int (*cmp) (const void*, const void*))
{
    if(root == NULL)
        return(CreateNodeBst(key, data));
    if(cmp(key, root->key) <= 0)
        root->left = AddBSTItem(root->left, key, data, cmp);
    else
        root->right = AddBSTItem(root->right, key, data, cmp);
    return root;
}

nodeBST* SearchBSTItem(nodeBST *root, void *key, int (*cmp) (const void*, const void*))
{
    int i;
    while(root != NULL)
    {
        i = cmp(root->key, key);
        if(i == 0)
        {
            return root;
        }
        if(i > 0)
        {
            root = root->left;
        }
        else
        {
            root = root->right;
        }
    }
    return NULL;
}

nodeBST* FindMinBST(nodeBST *root)
{
    while(root->left != NULL)
        root = root->left;
    return root;
}

nodeBST *DeleteBSTItem(nodeBST *root, void *key, int (*cmp) (const void*, const void*))
{
    nodeBST *n;
    n = SearchBSTItem(root, key, cmp);

    if(n != NULL)
    {
        n = CreateNodeBst(n->key, n->data);
        root = DeleteBSTItemReturnRoot(root, key, cmp);
    }
    return n;
}

nodeBST* DeleteBSTItemReturnRoot(nodeBST *root, void *key, int (*cmp) (const void*, const void*))
{
    nodeBST *n;
    if(root == NULL)
    {
        return root;
    }

    int comparare = cmp(key, root->key);
    if(comparare < 0)
    {
        root->left = DeleteBSTItemReturnRoot(root->left, key, cmp);
    }
    else if(comparare > 0)
    {
        root->right = DeleteBSTItemReturnRoot(root->right, key, cmp);
    }
    else
    {
        if(root->left == NULL)//are numai un fiu si anume pe partea dreapta
        {
            n = root->right;
            free(root);
            return n;
        }
        else if(root->right == NULL) //acum pe partea stg
        {
            n = root->left;
            free(root);
            return n;
        }

        n = FindMinBST(root->right);//2 fii
        root = CreateNodeBst(n->key, n->data);
        root->right = DeleteBSTItemReturnRoot(root->right, n->key, cmp);
    }
    return root;
}

int HeightBST(nodeBST *root)
{
    int lh;     //height of left subtree
    int rh;     // height of right subtree

    if(root == NULL)
    {
        return 0;
    }
    else
    {
        lh = HeightBST(root->left);
        rh = HeightBST(root->right);
        return 1 + (lh > rh ? lh : rh);
    }
}

void DeleteBST(nodeBST *root)
{
    if(root!=0)
    {
        DeleteBST(root->left);
        DeleteBST(root->right);
        free(root);
    }
}

void MergeBSTs(nodeBST *root1, nodeBST *root2, int (*cmp) (const void*, const void*))
{
    if(root2 == NULL)
    {
        return ;
    }
    MergeBSTs(root1, root2->left, cmp);
    AddBSTItem(root1, root2->key, root2->data, cmp);
    MergeBSTs(root1, root2->right, cmp);
}
*/
