#include <stdio.h>
#include <stdlib.h>
#define NMAX 100
/*Cea mai lungă subsecvență ordonată. Scrieți un program C care cere utilizatorului introducerea elementelor
naturale ale unui vector iar
programul va afişa cea mai lungă subsecvență ordonată de elemente consecutive din vector.
Citirea se încheie când se introduce un număr negativ. Afișarea se va face respectând șablonul dat mai jos.
Folosiți notația specifică pointerilor și expresii cu pointeri.
Se vor folosi doar variabile locale, funcţii şi un stil de programare adecvat.
Șablon pentru afișare:
-------------------------------------------------------------------------------
Introduceţi elementele vectorului: 7 1 6 2 5 6 7 8 4 3 5 6 -1
Secventa cea mai lunga ordonata crescator este: 2 5 6 7 8
@author: Denisa Barar
@date: 16-nov-2016
*/
int citire_vector(int v[NMAX])
{
    int n=0;
    printf("Introduceti elementele vectorului: \n");
    do
    {
        printf("v[%d]=",n);
        scanf("%d",&v[n++]);
    }
    while(v[n-1]>0);
    return --n;
}

void afisare_vector(int n,int v[NMAX],int i)
{
    for(;i<n;i++)
        printf("%d  ",v[i]);
}

void subsecventa_max(int n, int v[NMAX],int *i_max,int *nr_max)
{
    int nr=0;
    for(int i=0;i<n;i++)
    {
        nr=1;
        for(int j=i;v[j+1]>v[j];j++)
            nr++;
        if(nr>*nr_max)
        {
            *nr_max=nr;
            *i_max=i;
        }
    }
}

int main()
{
    int n;
    int v[NMAX];
    int i_max=0;
    int nr_max=0;
    n=citire_vector(v);
    afisare_vector(n,v,0);
    subsecventa_max(n,v,&i_max,&nr_max);
    printf("\nSecventa cea mai lunga ordonata crescator este: \n");
    afisare_vector(nr_max+i_max,v,i_max);
    return 0;
}
