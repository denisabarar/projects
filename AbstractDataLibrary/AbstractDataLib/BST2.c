#include "AbstractData.h"


BST* CreateBST(void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*))
{
    BST* b = malloc(sizeof(BST));
    b->root = NULL;
    b->cmp = cmp;
    b->print = print;
    return b;
}

nodeBST* CreateNodeBst(void *key, void *data)
{
    nodeBST* n = malloc(sizeof(nodeBST));
    n->key = key;
    n->data = data;
    n->left = NULL;
    n->right = NULL;

    return n;
}

void PrintBST(BST* b, FILE *f)
{
    PrintBSTRecursive(b->root, f, b->print, 0);
}
void PrintBSTRecursive(nodeBST *root, FILE *f, void (*print) (const void*, FILE*),int level)
{
    if(root == NULL)
    {
        return ;
    }

    PrintBSTRecursive(root->left, f, print, level + 1);
    for(int i = 0; i <= level; i++)
        fprintf(f, "  ");
    print(root->data, f);
    fprintf(f, "\n");
    PrintBSTRecursive(root->right, f, print, level + 1);
    /*int h = HeightBST(b);
    for(int i = 1; i <= h; i++)
    {
        PrintGivenLevel(root, f, print, i);
        fprintf(f, "\n");
    }*/
}

void PrintGivenLevel(nodeBST *root, FILE *f, void (*print) (const void*, FILE*), int level)
{
    if (root != NULL)
    {
        for(int i = 0; i <= level; i++)
            fprintf(f, " ");

        if (level == 1)
        {
            print(root->data, f);
        }
        else if (level > 1)
        {
            PrintGivenLevel(root->left, f, print, level-1);
            PrintGivenLevel(root->right, f, print, level-1);
        }
    }
}

void PreorderBST(BST* b, FILE *f)
{
    PreorderBSTRecursive(b->root, f, b->print);
    fprintf(f, "\n");
}

void InorderBST(BST* b, FILE *f)
{
    InorderBSTRecursive(b->root, f, b->print);
    fprintf(f, "\n");
}

void PostorderBST(BST* b, FILE *f)
{
    PostorderBSTRecursive(b->root, f, b->print);
    fprintf(f, "\n");
}

void PreorderBSTRecursive(nodeBST *root, FILE *f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    print(root->data, f);
    PreorderBSTRecursive(root->left, f, print);
    PreorderBSTRecursive(root->right, f, print);
}

void InorderBSTRecursive(nodeBST *root, FILE *f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    InorderBSTRecursive(root->left, f, print);
    print(root->data, f);
    InorderBSTRecursive(root->right, f, print);
}

void PostorderBSTRecursive(nodeBST *root, FILE *f, void (*print) (const void*, FILE*))
{
    if(root == NULL)
    {
        return ;
    }
    PostorderBSTRecursive(root->left, f, print);
    PostorderBSTRecursive(root->right, f, print);
    print(root->data, f);
}

void AddBSTItem(BST *b, void *key, void *data)
{
    b->root = AddBSTItemRecursive(b->root, key, data, b->cmp);
}
nodeBST* AddBSTItemRecursive(nodeBST *root, void *key, void *data, int (*cmp) (const void*, const void*))
{
    if(root == NULL)
        return(CreateNodeBst(key, data));
    if(cmp(key, root->key) <= 0)
        root->left = AddBSTItemRecursive(root->left, key, data, cmp);
    else
        root->right = AddBSTItemRecursive(root->right, key, data, cmp);
    return root;
}

nodeBST* SearchBSTItem(BST *b, void *key)
{
    nodeBST *n = SearchBSTItemRecursive(b->root, key, b->cmp);
    return n;
}
nodeBST* SearchBSTItemRecursive(nodeBST *root, void *key, int (*cmp) (const void*, const void*))
{
    int i;
    while(root != NULL)
    {
        i = cmp(root->key, key);
        if(i == 0)
        {
            return root;
        }
        if(i > 0)
        {
            root = root->left;
        }
        else
        {
            root = root->right;
        }
    }
    return NULL;
}

nodeBST* FindMinBST(nodeBST *root)
{
    while(root->left != NULL)
        root = root->left;
    return root;
}

nodeBST* DeleteBSTItem(BST *b, void *key)
{
    nodeBST *n = DeleteBSTItemRecursive(b->root, key, b->cmp);
    return n;
}
nodeBST *DeleteBSTItemRecursive(nodeBST *root, void *key, int (*cmp) (const void*, const void*))
{
    nodeBST *n;
    n = SearchBSTItemRecursive(root, key, cmp);

    if(n != NULL)
    {
        n = CreateNodeBst(n->key, n->data);
        root = DeleteBSTItemReturnRoot(root, key, cmp);
    }
    return n;
}

nodeBST* DeleteBSTItemReturnRoot(nodeBST *root, void *key, int (*cmp) (const void*, const void*))
{
    nodeBST *n;
    if(root == NULL)
    {
        return root;
    }

    int comparare = cmp(key, root->key);
    if(comparare < 0)
    {
        root->left = DeleteBSTItemReturnRoot(root->left, key, cmp);
    }
    else if(comparare > 0)
    {
        root->right = DeleteBSTItemReturnRoot(root->right, key, cmp);
    }
    else
    {
        if(root->left == NULL)//are numai un fiu si anume pe partea dreapta
        {
            n = root->right;
            free(root);
            return n;
        }
        else if(root->right == NULL) //acum pe partea stg
        {
            n = root->left;
            free(root);
            return n;
        }

        n = FindMinBST(root->right);//2 fii
        root = CreateNodeBst(n->key, n->data);
        root->right = DeleteBSTItemReturnRoot(root->right, n->key, cmp);
    }
    return root;
}

int HeightBST(BST *b)
{
    return HeightBSTRecursive(b->root);
}
int HeightBSTRecursive(nodeBST *root)
{
    int lh;     //height of left subtree
    int rh;     // height of right subtree

    if(root == NULL)
    {
        return 0;
    }
    else
    {
        lh = HeightBSTRecursive(root->left);
        rh = HeightBSTRecursive(root->right);
        return 1 + (lh > rh ? lh : rh);
    }
}

void DeleteBST(BST *b)
{
    DeleteBSTRecursive(b->root);
}
void DeleteBSTRecursive(nodeBST *root)
{
    if(root!=0)
    {
        DeleteBSTRecursive(root->left);
        DeleteBSTRecursive(root->right);
        free(root);
    }
}

void MergeBSTs(BST *b1, BST *b2)
{
    MergeBSTsRecursive(b1->root, b2->root, b1->cmp);
}
void MergeBSTsRecursive(nodeBST *root1, nodeBST *root2, int (*cmp) (const void*, const void*))
{
    if(root2 == NULL)
    {
        return ;
    }
    MergeBSTsRecursive(root1, root2->left, cmp);
    AddBSTItemRecursive(root1, root2->key, root2->data, cmp);
    MergeBSTsRecursive(root1, root2->right, cmp);
}
