#ifndef VECTORHEADER_H_INCLUDED
#define VECTORHEADER_H_INCLUDED

typedef struct Vector
{
    void **a;
    int sizev;
    int capacity;
    int expensionFactor;

    void (*print) (const void*, FILE*);
	int (*cmp) (const void*, const void*);
}Vector;

Vector* CreateVector(int capacity, void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*));
void PrintVector(Vector *v, FILE *f);
int AddVectorItem(Vector *v, void *item);
int AddVectorItems(Vector *v, void *items, int nrElemente,int type);
int PutVectorItem(Vector *v, void *item, int index);
void* GetVectorItem(Vector *v, int index);
int DeleteVectorItem(Vector *v, int index);
int SearchVectorItem(Vector *v, void *item);
void SortVector(Vector *v);
void DeleteVector(Vector *v);
int ExpandCapacity(Vector *v);
int MergeVectors(Vector *v, Vector *w);
#endif // VECTORHEADER_H_INCLUDED
