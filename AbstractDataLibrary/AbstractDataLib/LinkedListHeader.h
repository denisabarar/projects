#ifndef LINKEDLISTHEADER_H_INCLUDED
#define LINKEDLISTHEADER_H_INCLUDED

typedef struct node
{
    void *data;
    struct node *next;
}node;

typedef struct LinkedList
{
    node *head;
    node *tail;
    int sizel;

    void (*print) (const void*, FILE*);
	int (*cmp) (const void*, const void*);
}LinkedList;

node* CreateNode(void *data);
LinkedList* CreateLinkedList(void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*));
void PrintLinkedList(LinkedList *l, FILE *f);
int AddLinkedListItem(LinkedList *l, void *item);
int PutLinkedListItem(LinkedList *l, void *item, int index);
node* GetLinkedListItem(LinkedList *l, int index);
node* DeleteLinkedListItem(LinkedList *l, void *item);
node* SearchLinkedListItem(LinkedList *l, void *item);
void SortLinkedList(LinkedList *l);
int DeleteLinkedList(LinkedList *l);
int IsEmptyLinkedList(LinkedList *l);
int MergeLinkedLists(LinkedList *l, LinkedList *ll);

#endif // LINKEDLISTHEADER_H_INCLUDED
