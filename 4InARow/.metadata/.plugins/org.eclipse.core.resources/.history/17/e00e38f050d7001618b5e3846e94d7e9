
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class FourInARowModel extends JPanel{

	private static final int FRAME_THICKNESS = 16;
	private static final int GRID_WIDTH = 8;
	
	private static final long   serialVersionUID = 4019841629547494495L;
	private GameOverImage image;

	static Cell[][] grid = new Cell[8][8];
	
	static Cell.Player player;
	static Cell.Player winner;
	
	private boolean mouseActive = false;
	
	public FourInARowModel() {
		this.winner = Cell.Player.NONE;
		this.image = new GameOverImage(this);
        this.image.run();
		initializeGrid();
		initializePlayer();
	}

	public void initializeGrid() {
		int xx = FRAME_THICKNESS;
		for (int x = 0; x < GRID_WIDTH; x++) {
			int yy = FRAME_THICKNESS;
			for (int y = 0; y < GRID_WIDTH; y++) {
				Cell cell = new Cell(Cell.Player.NONE);
				cell.setCellLocation(xx, yy);
				grid[x][y] = cell;
				yy += FRAME_THICKNESS + Cell.getCellWidth();
			}
			xx += FRAME_THICKNESS + Cell.getCellWidth();
		}
	}
	
	public void initializePlayer(){
		player = Cell.Player.RED;
	}

	public Cell.Player getPlayer(){
		return player;
	}
	
	public Cell.Player alternatePlayer(Cell.Player player){
		return (player == Cell.Player.RED) ? Cell.Player.BLUE : Cell.Player.RED;
	}
	
	public void addCell(int y){
		if(!isGameOver()){
			if(!isGridFull()){
				if(mouseActive == true){
					for(int i = GRID_WIDTH - 1; i > 0; i--){
						if(grid[y][i].isZeroValue()){
							grid[y][i].setValue(player);
							player = alternatePlayer(player);
							break;
						}
					}
				}
			}
		}
	}
	
	public Cell.Player isWinnerOrizontally(){
		int k1;
		int k2;
		for(int i = 0; i < GRID_WIDTH; i++){
			k1 = 0;
			k2 = 0;
			for(int j = 0; j < GRID_WIDTH; j++){
				if(grid[i][j].isRed()){
					k1++;
				}
				else if(grid[i][j].isBlue()){
					k2++;
				}
				if(k1 == 4){
					winner = Cell.Player.RED;
				}
				else if(k2 == 4){
					winner = Cell.Player.BLUE;
				}
			}
		}
		return winner;				
	}
	
	public Cell.Player isWinnerVertically(){
		int k1;
		int k2;
		for(int i = 0; i < GRID_WIDTH; i++){
			k1 = 0;
			k2 = 0;
			for(int j = 0; j < GRID_WIDTH; j++){
				if(grid[j][i].isRed()){
					k1++;
				}
				else if(grid[j][i].isBlue()){
					k2++;
				}
				if(k1 == 4){
					winner = Cell.Player.RED;
				}
				else if(k2 == 4){
					winner = Cell.Player.BLUE;
				}
			}
		}
		return winner;				
	}
	
	public void isWinner(){
		if(isWinnerOrizontally() != Cell.Player.NONE || isWinnerVertically() != Cell.Player.NONE){
			mouseActive = false;
		}
			
	}
	
	public Cell getCell(int x, int y) {
		return grid[x][y];
	}

	public boolean isGameOver() {
		return isGridFull();
	}

	private boolean isGridFull() {
		for (int x = 0; x < GRID_WIDTH; x++) {
			for (int y = 0; y < GRID_WIDTH; y++) {
				if (grid[x][y].isZeroValue()) {
					return false;
				}
			}
		}
		return true;
	}

	public void setMouseActive(boolean mouseActive){
		this.mouseActive = mouseActive;
	}
	
	public boolean getMouseActive(){
		return mouseActive;
	}
	
	
	public Dimension getPreferredSize() {
		int width = GRID_WIDTH * Cell.getCellWidth() + FRAME_THICKNESS * 9;
		return new Dimension(width, width);
	}

	public void draw(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		Dimension d = getPreferredSize();
		g.fillRect(0, 0, d.width, d.height);

		for (int x = 0; x < GRID_WIDTH; x++) {
			for (int y = 0; y < GRID_WIDTH; y++) {
				{
					grid[x][y].draw(g);
				}
			}
		}
	}
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.draw(g);
         
        if (this.isGameOver()) {
            g.drawImage(image.getImage(), 0, 0, null);
        }
    }
	
	public int getSizeCell(){
		return GRID_WIDTH;
	}
	
	public int getWidth(){
		return GRID_WIDTH * Cell.getCellWidth() + FRAME_THICKNESS * 9;
	}
}
