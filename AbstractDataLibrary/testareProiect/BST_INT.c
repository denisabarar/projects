#include "testerHeader.h"

void BST_Int(int argc, char** argv, int indice)//indice = al catelea fisier din argv
{
   /* fisiere f;
    determineFiles(argc,argv,indice,&f);

    char comanda[30];
    char c, k;
    nodeBST *b, *bb;

    VectorStructures vs[10];
    int sizevs = 0;

    while(!feof(f.f))
    {
        fscanf(f.f,"%s %c ",comanda,&c);

        if(!strcmp(comanda, "CreateBST"))
        {
            b = CreateBST();
            vs[sizevs].structura = &b;
            vs[sizevs].denumire = c;
            sizevs++;
        }
        else if(!strcmp(comanda, "PrintBST"))
        {
            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PrintBST(b, f.g, printInt,0);
            }
            fprintf(f.g, "\n");
        }
        else if(!strcmp(comanda, "PreorderBST"))
        {
            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PreorderBST(b, f.g, printInt);
            }
            fprintf(f.g, "\n");
        }
        else if(!strcmp(comanda, "InorderBST"))
        {
            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                InorderBST(b, f.g, printInt);
            }
            fprintf(f.g, "\n");
        }
        else if(!strcmp(comanda, "PostorderBST"))
        {
            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                PostorderBST(b, f.g, printInt);
            }
            fprintf(f.g, "\n");
        }
        else if(!strcmp(comanda, "AddBSTItem"))
        {
            void *data;
            void *key;

            data = (int*)malloc(sizeof(void*));
            key = (int*)malloc(sizeof(void*));
            fscanf(f.f,"%d ",(int*)data);
            *((int*)key) = *((int*)data);

            void *p = determinaStructura(vs, sizevs, c);
            if(p != NULL)
            {
                if(b != NULL)
                    b = *(nodeBST**)determinaStructura(vs, sizevs, c);
                b = AddBSTItem(b, key, data, cmpNumbers);
                updateVS(b, vs, sizevs, c);
            }
        }
        else if(!strcmp(comanda, "HeightBST"))
        {
            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            if(b != NULL)
                fprintf(f.g, "%d\n", HeightBST(b));
        }
        else if(!strcmp(comanda, "DeleteBSTItem"))
        {
            void* key = malloc(sizeof(void*));
            nodeBST *n;

            fscanf(f.f,"%d ",(int*)key);

            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            if(b != NULL)
                n = DeleteBSTItem(b, key, cmpNumbers);
            if(n != NULL)
                    fprintf(f.g,"%d \n",*((int*)n->data));
                else
                    fprintf(f.g, "Elementul nu a fost gasit ca sa fie sters!\n");
        }
        else if(!strcmp(comanda, "SearchBSTItem"))
        {
            void* key = malloc(sizeof(void*));
            nodeBST *n = CreateNodeBst(NULL, NULL);

            fscanf(f.f,"%d ",(int*)key);

            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                n = SearchBSTItem(b, key, cmpNumbers);
                if(n != NULL)
                    fprintf(f.g,"%d \n",*((int*)n->data));
                else
                    fprintf(f.g, "Elementul nu a fost gasit!\n");
            }
        }
        else if(!strcmp(comanda, "DeleteBST"))
        {
            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            if(b != NULL)
            {
                DeleteBST(b);
                for(int i=0; i < sizevs; i++)
                {
                    if(vs[i].denumire == c)
                    {
                        vs[i].structura = NULL;
                        break;
                    }
                }
            }
        }
        else if(!strcmp(comanda, "MergeBSTs"))
        {
            fscanf(f.f,"%c ",&k);
            b = *(nodeBST**)determinaStructura(vs, sizevs, c);
            bb = *(nodeBST**)determinaStructura(vs, sizevs, k);
            if(b != NULL && bb != NULL)
            {
                MergeBSTs(b, bb, cmpNumbers);
            }
        }
    }
    fclose(f.f);
    fclose(f.g);*/
}
void updateVS(nodeBST *b, VectorStructures vs[10], int sizevs, char c)
{
    int i = determinaPozBST(vs, sizevs, c);
    if(i != -1)
    {
        vs[i].structura = b;
    }
}
