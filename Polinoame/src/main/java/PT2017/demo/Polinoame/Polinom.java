package PT2017.demo.Polinoame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom{

	private List <Monom> monoame;

	public Polinom(){
		monoame = new ArrayList<Monom>();	
	}

	public Polinom(String s){
		monoame = new ArrayList<Monom>();		
		Pattern pattern = Pattern.compile("([+-]?\\d*)x(\\^([-]?\\d+))?|([+-]?\\d+)");
		Matcher m = pattern.matcher(s);
	
		int p, c;
		while(m.find()){
			if(m.group(4) == null){//for power				
				if(m.group(3) == null)
					p = 1;
				else
					p = Integer.parseInt(m.group(3));//for coefficient				
				if(m.group(1).equals(" ") || m.group(1).equals("+"))
					c = 1;
				else if(m.group(1).equals("-"))
					c = -1;
				else
					c = Integer.parseInt(m.group(1));;
			}
			else{
				p = 0;
				c = Integer.parseInt(m.group(4));
			}
			monoame.add(new Monom(c,p));				
		}
	
		Collections.sort(monoame);
		Collections.reverse(monoame);
	}
	
	public String toString(){
		String s = new String("");
		Iterator<Monom> i = monoame.iterator();
		if(i.hasNext()){
			s = s + i.next();
		}
		while(i.hasNext()){
			Monom m = i.next();
			if(m.getCoefficient() > 0)
				s = s + "+";
			s = s + m;
		}
		
		if(s.equals("")){
			s += "0";
		}
		
		return s;
	}
	
	public int degree(){
		Collections.sort(this.monoame);
		Collections.reverse(this.monoame);
		if(!monoame.isEmpty())
			return monoame.get(0).getPower();
		else
			return 0;
	}
	
	public void resetCoefficientLargestDegree(){
		Collections.sort(this.monoame);
		Collections.reverse(this.monoame);
		monoame.remove(monoame.get(0));
	}
	
	public float getFirstCoefficient(){
		try{
			return this.getMonoame().get(0).getCoefficient();
		}
		catch(Exception e){
			return 0;
		}
	}
	
	public void addMonom(float coefficient, int power){
		boolean ok = false;
		for(Monom m : monoame){
			if(m.getPower() == power){
				if(m.getCoefficient() + coefficient == 0){
					monoame.remove(monoame.indexOf(m));
				}
				else{
					monoame.set(monoame.indexOf(m), new Monom(coefficient + m.getCoefficient(), power));
				}	
				ok = true;
				break;
			}
		}
		if(ok == false)
			monoame.add(new Monom(coefficient, power));
	}
	
	
	public List <Monom> getMonoame() {
		return monoame;
	}

	public void setMonoame(List <Monom> monoame) {
		this.monoame = monoame;
	}
}
