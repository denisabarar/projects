
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.event.*;

public class FourInARowFrame{
	
	private FourInARowModel model;
	private JFrame frame;
	private ControlPanel controlPanel;
	private JPanel mainPanel;

	public FourInARowFrame(FourInARowModel model) {
		this.model = model;
		createPartControl();
		
	}

	private void createPartControl() {

		controlPanel = new ControlPanel(this, model);

		frame = new JFrame();
		frame.setTitle("FourInARow");
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				exitProcedure();
			}
		});

		mainPanel = new JPanel();
		mainPanel.setLayout(new FlowLayout());
		mainPanel.add(model);
		mainPanel.add(createSidePanel());

		frame.add(mainPanel);
		frame.setLocationByPlatform(true);
		frame.pack();
		frame.setVisible(true);
		
		addClickMouseListener();
	}
		
	private JPanel createSidePanel() {
		JPanel sidePanel = new JPanel();
		sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.PAGE_AXIS));
		sidePanel.add(Box.createVerticalStrut(30));
		sidePanel.add(controlPanel.getPanel());
		return sidePanel;
	}
	
	public void updatePlayer(){
		Cell.Player player = model.getPlayer();
		if(player == Cell.Player.RED){
			controlPanel.getLabel1().setForeground(Color.RED);
			controlPanel.getLabel2().setForeground(Color.GRAY);
		}
		if(player == Cell.Player.BLUE){
			controlPanel.getLabel1().setForeground(Color.GRAY);
			controlPanel.getLabel2().setForeground(Color.BLUE);
		}
	}

	public void exitProcedure() {
		frame.dispose();
		System.exit(0);
	}

	public void repaintGridPanel() {
		model.repaint();
	}

	public void addClickMouseListener(){
		MouseClickListener listener = new MouseClickListener(this, this.model);
		mainPanel.addMouseListener(listener);
	}
	
}
