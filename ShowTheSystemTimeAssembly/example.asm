.386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
extern malloc: proc
extern memset: proc
extern time:proc
extern ctime:proc
extern printf: proc

includelib canvas.lib
extern BeginDrawing: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
;aici declaram date
window_title DB "Ceas Digital",0
area_width EQU 340
area_height EQU 240
area DD 0

counter DD 0 ; numara evenimentele de tip timer

arg1 EQU 8
arg2 EQU 12
arg3 EQU 16
arg4 EQU 20

S_AN equ 31556926
S_ZI equ 86400
S_ORA equ 3600
S_MIN equ 60
DECALAJ equ 19456

timeptr dd 0
formatD db "%d ",0

an dd 0
nrZile dd 0
ora dd 0
min dd 0
ora1 dd 0
ora2 dd 0
min1 dd 0
min2 dd 0
sec dd 0
sec1 dd 0
sec2 dd 0

symbol_width EQU 10
symbol_height EQU 20
include digits.inc
include letters.inc

.code
; procedura make_text afiseaza o litera sau o cifra la coordonatele date
; arg1 - simbolul de afisat (litera sau cifra)
; arg2 - pointer la vectorul de pixeli
; arg3 - pos_x
; arg4 - pos_y
make_text proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax, 'A'
	jl make_digit
	cmp eax, 'Z'
	jg make_digit
	sub eax, 'A'
	lea esi, letters
	jmp draw_text
make_digit:
	cmp eax, '0'
	jl make_space
	cmp eax, '9'
	jg make_space
	
	cmp eax, ':'
	je make_2dots
	
	sub eax, '0'
	lea esi, digits
	jmp draw_text
make_space:	
	mov eax, 26 ; de la 0 pana la 25 sunt litere, 26 e space
	lea esi, letters
make_2dots:
	mov eax, 27
	lea esi, letters
	
draw_text:
	mov ebx, symbol_width
	mul ebx
	mov ebx, symbol_height
	mul ebx
	add esi, eax
	mov ecx, symbol_height
bucla_simbol_linii:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_width
bucla_simbol_coloane:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb
	mov dword ptr [edi], 0
	jmp simbol_pixel_next
simbol_pixel_alb:
	mov dword ptr [edi], 0FFFFFFh
simbol_pixel_next:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane
	pop ecx
	loop bucla_simbol_linii
	popa
	mov esp, ebp
	pop ebp
	ret
make_text endp

; un macro ca sa apelam mai usor desenarea simbolului
make_text_macro macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call make_text
	add esp, 16
endm

; functia de desenare - se apeleaza la fiecare click
; sau la fiecare interval de 200ms in care nu s-a dat click
; arg1 - evt (0 - initializare, 1 - click, 2 - s-a scurs intervalul fara click)
; arg2 - x
; arg3 - y
draw proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1]
	cmp eax, 2
	jz event_timer;;;;;;;
	
	
	;mai jos e codul care intializeaza fereastra cu pixeli albi
	mov eax, area_width
	mov ebx, area_height
	;mov edx,
	mul ebx
	shl eax, 2
	push eax
	push 039eb39h
	push area
	call memset
	add esp, 12
	jmp afisare_litere
	

	
	
event_timer:	;;;;;;;;
	mov eax,0	
	mov ebx,0
	mov ecx,0
	mov edx,0
	
	;determina numarul de secunde cu functia time
	push offset sec
	call time
	add esp,4
		
	;add decalaj
	mov eax, sec
	add eax, DECALAJ
	mov sec, eax
	
	
	;determinare an
	mov edx, 0
	mov eax, sec
	mov ecx, S_AN
	div ecx
	add eax, 1970
	mov an,eax
	
	;restul
	mov sec, edx
	
	;determina numar de zile
	mov edx, 0
	mov eax, sec
	mov ecx, S_ZI
	div ecx
	inc eax
	mov nrZile, eax
	
	;restul
	mov sec, edx
	
	;determina ora
	mov edx, 0
	mov eax, sec
	mov ecx, S_ORA
	div ecx
	mov ora, eax
	
	;restul
	mov sec, edx
	
	;determinare minute
	mov edx, 0
	mov eax, sec
	mov ecx, S_MIN
	div ecx
	mov min,eax
	
	;restul=> chiar numarul de secunde
	mov sec, edx



	;;;;;afisare ora;;;;;
	mov edx,0
	mov eax,ora
	mov ecx,0
	mov ecx,10
	div ecx
	mov ora1,eax
	
	
	cmp ora1,0
	je _0
	cmp ora1,1
	je _1
	cmp ora1,2
	je _2
	cmp ora1,3
	je _3
	cmp ora1,4
	je _4
	cmp ora1,5
	je _5
	cmp ora1,6
	je _6
	cmp ora1,7
	je _7
	cmp ora1,8
	je _8
	cmp ora1,9
	je _9

_0:
mov ora1,'0'
jmp fexit1
_1:
mov ora1,'1'
jmp fexit1
_2:
mov ora1,'2'
jmp fexit1
_3:
mov ora1,'3'
jmp fexit1
_4:
mov ora1,'4'
jmp fexit1
_5:
mov ora1,'5'
jmp fexit1
_6:
mov ora1,'6'
jmp fexit1
_7:
mov ora1,'7'
jmp fexit1
_8:
mov ora1,'8'
jmp fexit1
_9:
mov ora1,'9'
jmp fexit1

fexit1:
	mov edx,0
	mov eax,ora
	mov ecx,0
	mov ecx,10
	div ecx
	mov ora2,edx

	cmp ora2,0
	je _00
	cmp ora2,1
	je _11
	cmp ora2,2
	je _22
	cmp ora2,3
	je _33
	cmp ora2,4
	je _44
	cmp ora2,5
	je _55
	cmp ora2,6
	je _66
	cmp ora2,7
	je _77
	cmp ora2,8
	je _88
	cmp ora2,9
	je _99

_00:
mov ora2,'0'
jmp fexit2
_11:
mov ora2,'1'
jmp fexit2
_22:
mov ora2,'2'
jmp fexit2
_33:
mov ora2,'3'
jmp fexit2
_44:
mov ora2,'4'
jmp fexit2
_55:
mov ora2,'5'
jmp fexit2
_66:
mov ora2,'6'
jmp fexit2
_77:
mov ora2,'7'
jmp fexit2
_88:
mov ora2,'8'
jmp fexit2
_99:
mov ora2,'9'
jmp fexit2



;;;;;;;;;;afisare minute;;;;;;;;;;;
fexit2:
	mov edx,0
	mov eax,min
	mov ecx,0
	mov ecx,10
	div ecx
	mov min1,eax
	
	
	cmp min1,0
	je _0m
	cmp min1,1
	je _1m
	cmp min1,2
	je _2m
	cmp min1,3
	je _3m
	cmp min1,4
	je _4m
	cmp min1,5
	je _5m
	cmp min1,6
	je _6m
	cmp min1,7
	je _7m
	cmp min1,8
	je _8m
	cmp min1,9
	je _9m

_0m:
mov min1,'0'
jmp fexit1m
_1m:
mov min1,'1'
jmp fexit1m
_2m:
mov min1,'2'
jmp fexit1m
_3m:
mov min1,'3'
jmp fexit1m
_4m:
mov min1,'4'
jmp fexit1m
_5m:
mov min1,'5'
jmp fexit1m
_6m:
mov min1,'6'
jmp fexit1m
_7m:
mov min1,'7'
jmp fexit1m
_8m:
mov min1,'8'
jmp fexit1m
_9m:
mov min1,'9'
jmp fexit1m

fexit1m:
	mov edx,0
	mov eax,min
	mov ecx,0
	mov ecx,10
	div ecx
	mov min2,edx

	cmp min2,0
	je _00m
	cmp min2,1
	je _11m
	cmp min2,2
	je _22m
	cmp min2,3
	je _33m
	cmp min2,4
	je _44m
	cmp min2,5
	je _55m
	cmp min2,6
	je _66m
	cmp min2,7
	je _77m
	cmp min2,8
	je _88m
	cmp min2,9
	je _99m

_00m:
mov min2,'0'
jmp fexit2m
_11m:
mov min2,'1'
jmp fexit2m
_22m:
mov min2,'2'
jmp fexit2m
_33m:
mov min2,'3'
jmp fexit2m
_44m:
mov min2,'4'
jmp fexit2m
_55m:
mov min2,'5'
jmp fexit2m
_66m:
mov min2,'6'
jmp fexit2m
_77m:
mov min2,'7'
jmp fexit2m
_88m:
mov min2,'8'
jmp fexit2m
_99m:
mov min2,'9'
jmp fexit2m



;;;;;afisare secunde;;;;;
fexit2m:
	mov edx,0
	mov eax,sec
	mov ecx,0
	mov ecx,10
	div ecx
	mov sec1,eax
	
	
	cmp sec1,0
	je _0s
	cmp sec1,1
	je _1s
	cmp sec1,2
	je _2s
	cmp sec1,3
	je _3s
	cmp sec1,4
	je _4s
	cmp sec1,5
	je _5s
	cmp sec1,6
	je _6s
	cmp sec1,7
	je _7s
	cmp sec1,8
	je _8s
	cmp sec1,9
	je _9s

_0s:
mov sec1,'0'
jmp fexit1s
_1s:
mov sec1,'1'
jmp fexit1s
_2s:
mov sec1,'2'
jmp fexit1s
_3s:
mov sec1,'3'
jmp fexit1s
_4s:
mov sec1,'4'
jmp fexit1s
_5s:
mov sec1,'5'
jmp fexit1s
_6s:
mov sec1,'6'
jmp fexit1s
_7s:
mov sec1,'7'
jmp fexit1s
_8s:
mov sec1,'8'
jmp fexit1s
_9s:
mov sec1,'9'
jmp fexit1s

fexit1s:
	mov edx,0
	mov eax,sec
	mov ecx,0
	mov ecx,10
	div ecx
	mov sec2,edx

	cmp sec2,0
	je _00s
	cmp sec2,1
	je _11s
	cmp sec2,2
	je _22s
	cmp sec2,3
	je _33s
	cmp sec2,4
	je _44s
	cmp sec2,5
	je _55s
	cmp sec2,6
	je _66s
	cmp sec2,7
	je _77s
	cmp sec2,8
	je _88s
	cmp sec2,9
	je _99s

_00s:
mov sec2,'0'
jmp fexit2s
_11s:
mov sec2,'1'
jmp fexit2s
_22s:
mov sec2,'2'
jmp fexit2s
_33s:
mov sec2,'3'
jmp fexit2s
_44s:
mov sec2,'4'
jmp fexit2s
_55s:
mov sec2,'5'
jmp fexit2s
_66s:
mov sec2,'6'
jmp fexit2s
_77s:
mov sec2,'7'
jmp fexit2s
_88s:
mov sec2,'8'
jmp fexit2s
_99s:
mov sec2,'9'
jmp fexit2s
fexit2s:
	
	
	
afisare_litere:

	;scriem un mesaj
	make_text_macro ora1, area, 110, 100
	make_text_macro ora2, area, 120, 100
	make_text_macro ':', area, 130, 100
	
	make_text_macro min1, area, 140, 100
	make_text_macro min2, area, 150, 100
	make_text_macro ':', area, 160, 100
	
	make_text_macro sec1, area, 170, 100
	make_text_macro sec2, area, 180, 100
	

final_draw:
	popa
	mov esp, ebp
	pop ebp
	ret
	
draw endp

start:
	;alocam memorie pentru zona de desenat
	

	
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	call malloc
	add esp, 4
	mov area, eax
	;apelam functia de desenare a ferestrei
	; typedef void (*DrawFunc)(int evt, int x, int y);
	; void __cdecl BeginDrawing(const char *title, int width, int height, unsigned int *area, DrawFunc draw);
	push offset draw
	push area
	push area_height
	push area_width
	push offset window_title
	call BeginDrawing
	add esp, 20
	
	;terminarea programului
	push 0
	call exit
end start
