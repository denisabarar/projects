package game2048;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
  
public class LeftArrowAction extends AbstractAction {
 
    private static final long serialVersionUID = 863330348471372562L;
 
    private Game2048Frame frame;
     
    private Game2048Model model;
 
    public LeftArrowAction(Game2048Frame frame, Game2048Model model) {
        this.frame = frame;
        this.model = model;
    }
     
    @Override
    public void actionPerformed(ActionEvent e) {
        if (model.isArrowActive()) {
            if (model.moveCellsLeft()) {
                if (model.isGameOver()) {
                    model.setArrowActive(false);
                } else {
                    model.addNewCell();
                     
                    frame.repaintGridPanel();
                    frame.updateScorePanel();
                }
            }
        }
    }
 
}