#ifndef BALANCEDBST_H_INCLUDED
#define BALANCEDBST_H_INCLUDED

typedef struct nodeAVL
{
    void *key;
    void *data;

    int height;

    struct nodeAVL *left;
    struct nodeAVL *right;
}nodeAVL;

typedef struct BalancedBST
{
    nodeAVL *root;
    void (*print) (const void*, FILE*);
	int (*cmp) (const void*, const void*);
}BalancedBST;

BalancedBST* CreateBalancedBST(void (*print) (const void*, FILE*), int (*cmp) (const void*, const void*));

void PrintBalancedBST(BalancedBST* b, FILE *f);
void PrintBalancedBSTRecursive(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*),int level);

void PreorderBalancedBST(BalancedBST* b, FILE *f);
void PreorderBalancedBSTRecursive(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*));
void InorderBalancedBST(BalancedBST* b, FILE *f);
void InorderBalancedBSTRecursive(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*));
void PostorderBalancedBST(BalancedBST* b, FILE *f);
void PostorderBalancedBSTRecursive(nodeAVL* root, FILE* f, void (*print) (const void*, FILE*));

void AddBalancedBSTItem(BalancedBST *b, void *key, void *data);
nodeAVL* AddBalancedBSTItemRecursive(nodeAVL* root, void *key, void *data, int (*cmp) (const void*, const void*));

nodeAVL* SearchBalancedBSTItem(BalancedBST *b, void *key);
nodeAVL* SearchBalancedBSTItemRecursive(nodeAVL* root, void *key, int (*cmp) (const void*, const void*));

nodeAVL* DeleteBalancedBSTItem(BalancedBST *b, void *key);
nodeAVL* DeleteBalancedBSTItemRecursive(nodeAVL* root, void *key, int (*cmp) (const void*, const void*));

int HeightBalancedBST(BalancedBST *b);
int HeightBalancedBSTRecursive(nodeAVL* root);

void DeleteBalancedBST(BalancedBST *b);
void DeleteBalancedBSTRecursive(nodeAVL* root);

void MergeBalancedBSTs(BalancedBST *b1, BalancedBST *b2);
void MergeBalancedBSTsRecursive(nodeAVL *root1, nodeAVL *root2, int (*cmp) (const void*, const void*));

nodeAVL* CreateNodeAVL(void *key, void *data);
nodeAVL* BalanceAVL(nodeAVL* root);
int BalanceFactorAVL(nodeAVL *root);
nodeAVL* RotateLeftRightAVL(nodeAVL* root);
nodeAVL* RotateLeftLeftAVL(nodeAVL* root);
nodeAVL* RotateRightLeftAVL(nodeAVL* root);
nodeAVL* RotateRightRightAVL(nodeAVL* root);
nodeAVL* DeleteBalancedBSTItemReturnRoot(nodeAVL* root, void *key, int (*cmp) (const void*, const void*));
nodeAVL* FindMinAVL(nodeAVL* root);

#endif // BALANCEDBST_H_INCLUDED
/*BalancedBST (implementat folosind o abordare la alegere pentru reechilibrarea arborelui – AVL, AA, B, etc.)
Operații
a. CreateBalancedBST - creare instanță BalancedBST
b. PrintBalancedtBST – afișarea elementelor sub formă de arbore
c. PreorderBalancedBST – parcurgere și afișare în preordine
d. InorderBalancedBST – parcurgere și afișare în inordine
e. PostorderBalancedBST – parcurgere și afișare în postordine
f. AddBalancedBSTItem – adăugarea unui nou element la arbore
g. SearchBalancedBSTItem – returnează elementul care are valoarea căutată
h. DeleteBalancedBSTItem – ștergerea și returnarea elementului cu valoarea specificată
i. HightBalancedBST – returnează înălțimea subarborelui specificat
j. DeleteBalancedBST – ștergerea elementelor și eliberarea memoriei*/
