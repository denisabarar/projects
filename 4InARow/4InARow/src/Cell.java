
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


public class Cell {

	enum Player {NONE, RED, BLUE};
	
	private static final int CELL_WIDTH = 60;

	private Player value;
	private Point cellLocation;
	
	
	public Cell(Player value) {
		setValue(value);
	}

	public static int getCellWidth() {
		return CELL_WIDTH;
	}

	public Player getValue() {
		return value;
	}

	public void setValue(Player value) {
		this.value = value;
	}

	public boolean isZeroValue() {
		return (value == Player.NONE);
	}
	
	public boolean isRed(){
		return (value == Player.RED);
	}

	public boolean isBlue(){
		return (value == Player.BLUE);
	}
	
	public boolean equalsTo(Cell.Player player){
		return (value == player);
	}
	
	public void setCellLocation(int x, int y) {
		setCellLocation(new Point(x, y));
	}

	public void setCellLocation(Point cellLocation) {
		this.cellLocation = cellLocation;
	}

	public void draw(Graphics g) {
		if (value == Player.NONE) {
			g.setColor(Color.GRAY);
			g.fillOval(cellLocation.x, cellLocation.y, CELL_WIDTH, CELL_WIDTH);
		} 
		if (value == Player.RED)
		{
			g.setColor(Color.RED);
			g.fillOval(cellLocation.x, cellLocation.y, CELL_WIDTH, CELL_WIDTH);
		}
		if (value == Player.BLUE)
		{
			g.setColor(Color.BLUE);
			g.fillOval(cellLocation.x, cellLocation.y, CELL_WIDTH, CELL_WIDTH);
		}
	}

}
