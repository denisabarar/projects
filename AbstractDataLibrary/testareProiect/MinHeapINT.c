#include "testerHeader.h"

void MinHeapInt(int argc, char** argv, int indice, int TYPE)//indice = al catelea fisier din argv
{
    fisiere f;
    determineFiles(argc,argv,indice,&f);

    char comanda[30];
    char c, k;
    MinHeap *v, *vv;

    VectorStructures vs[10];
    int sizevs = 0;

    while(!feof(f.f))
    {
        fscanf(f.f,"%s %c ",comanda,&c);

        if(!strcmp(comanda, "CreateHeap"))
        {
            v = CreateHeap(15,printInt,cmpNumbers);
            vs[sizevs].structura = v;
            vs[sizevs].denumire = c;
            sizevs++;
        }
        else if(!strcmp(comanda, "PrintHeap"))
        {
            v = (MinHeap*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
            {
                PrintHeap(v, f.g);
            }
        }
        else if(!strcmp(comanda, "AddHeapItem"))
        {
            void *item;

            v = (MinHeap*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
            {
                item=(int*)malloc(TYPE);
                fscanf(f.f,"%d ",(int*)item);
                AddHeapItem(v,item);
            }
        }
        else if(!strcmp(comanda, "GetHeapMin"))
        {
            void *item=(int*)malloc(TYPE);
            v = (MinHeap*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
            {
                item = (int*)GetHeapMin(v);
                if(item != NULL)
                    fprintf(f.g,"%d \n",*((int*)item));
                else
                    fprintf(f.g, "Elementul luat este NULL!\n");
            }
        }
        else if(!strcmp(comanda, "DeleteHeapMin"))
        {
            void *item=(int*)malloc(TYPE);
            v = (MinHeap*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
                item = DeleteHeapMin(v);
            if(item != NULL)
                fprintf(f.g,"%d \n",*((int*)item));
            else
                fprintf(f.g, "Elementul luat este NULL!\n");
        }
        else if(!strcmp(comanda, "DeleteHeapItem"))
        {
            void *item=(int*)malloc(TYPE);

            fscanf(f.f,"%d ",(int*)item);

            v = (MinHeap*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
            {
                DeleteHeapItem(v,item);
            }
        }
        else if(!strcmp(comanda, "SearchHeapItem"))
        {
            void *item=(int*)malloc(TYPE);

            fscanf(f.f,"%d ",(int*)item);

            int i = 0;
            v = (MinHeap*)determinaStructura(vs,sizevs, c);
            if(v != NULL)
            {
                i = SearchHeapIndex(v,item);
                if(i == -1)
                    fprintf(f.g, "Elementul nu a fost gasit!\n");
                else
                    fprintf(f.g, "Elementul a fost gasit!\n");
            }
        }
        else if(!strcmp(comanda, "DeleteHeap"))
        {
            v = (MinHeap*)determinaStructura(vs, sizevs, c);
            if(v != NULL)
            {
                DeleteHeap(v);
                for(int i=0; i < sizevs;i++)
                {
                    if(vs[i].denumire == c)
                    {
                        vs[i].structura = NULL;
                        break;
                    }
                }
            }
        }
        else if(!strcmp(comanda, "MergeMinHeaps"))
        {
            fscanf(f.f,"%c ",&k);
            v = (MinHeap*)determinaStructura(vs, sizevs, c);
            vv = (MinHeap*)determinaStructura(vs, sizevs, k);
            if(v != NULL)
            {
                MergeMinHeaps(v,vv);
            }
        }
        else
        {
            fprintf(f.g, "Comanda invalida!\n");
            fscanf(f.f,"%c ",&k);
        }
    }
    fclose(f.f);
    fclose(f.g);
}
